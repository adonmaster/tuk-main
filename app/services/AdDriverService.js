const _debounce = require('lodash/debounce')
const _omit = require('lodash/omit')
const Repo = require('../repos/Repo')
const Geo = require("../helpers/Geo");
const MobileService = require("./MobileService");
const Dt = require("../helpers/Dt");
const SessionService = require("./SessionService");
const CacheService = require("./CacheService");


let self
const AdDriverService = self = {

    async init() {
        const list = await Repo.ad.getActiveWithoutRoute()
        CacheService.adsActive = {}
        for (const ad of list) {
            CacheService.adsActive[ad.id] = ad
        }
    },

    /**
     *
     * @param {Ad|null} ad
     */
    notify(ad=null) {
        if (ad) CacheService.adsActive[ad.id] = _omit(ad, ['route'])
        self.__notify()
    },


    /**
     *
     */
    __notify: _debounce(async ()=> {

        console.log('-----------> AdDriverService@notify initializing...')

        const ads = Object.values(CacheService.adsActive)

        try {
            for (const ad of ads) {

                // finding drivers
                const oldDriverIds = await Repo.adDriver.getFromAd_DriverIds(ad.id)
                if (oldDriverIds.length > 10) break

                // try 1, .3 close
                const exclusionList = oldDriverIds
                const newDriverIds = await Repo.driver.getActiveIdsOrderByPts(ad.origin_lat, ad.origin_lng, Geo.FACTOR_3, 100, exclusionList)
                if (newDriverIds.length < 4) {

                    exclusionList.push(...newDriverIds)

                    // try 2, .7 close
                    let list = await Repo.driver.getActiveIdsOrderByPts(ad.origin_lat, ad.origin_lng, Geo.FACTOR_7, 50, exclusionList)
                    newDriverIds.push(...list)

                    if (newDriverIds.length < 6) {

                        // last try, furthest
                        exclusionList.push(...list)
                        list = await Repo.driver.getActiveIdsOrderByPts(ad.origin_lat, ad.origin_lng, Geo.FACTOR, 15, exclusionList)

                        newDriverIds.push(...list)
                    }
                }

                //
                if (newDriverIds.length == 0) break

                //
                let userIds = await Repo.driver.driverIdsToUserIds(newDriverIds)
                userIds = userIds.filter(id => ad.user_id != id) // exclude ad user_id
                const body = `${ad.user_name} precisa de sua ajuda!`
                await MobileService.send(userIds, 'Corrida!', body, 'ad')

                //
                await Repo.adDriver.registerDriverIds(ad.id, newDriverIds)
            }
        } catch (e) {
            loge('-----------> AdDriverService:ERROR:' + e.message, {AdDriverService_id: ads})
        }

    }, 2100, {leading: true, trailing: true}),


    async cronCleanUp()
    {
        console.log('-----------> AdDriverService@cronCleanUp initializing...')

        const adTimeout = parseInt(process.env.AD_TIMEOUT)
        const adIdsToDelete = Object.values(CacheService.adsActive)
            .filter(i => Dt.now().diff(moment(i.created_at), 'minute') > adTimeout)
            .map(({id}) => id);

        //
        if (adIdsToDelete.length == 0) return

        //
        await self.invalidate(adIdsToDelete)
    },


    /**
     *
     * @param {int[]} adIds
     * @returns {Promise<void>}
     */
    async invalidate(adIds=[0])
    {
        try {

            //
            let adIdsToDelete = adIds.includes(0) ? Object.keys(CacheService.adsActive) : adIds

            let driverIds = []
            let adUserIds = []
            await Repo.transaction(async t => {

                // remove ad from db
                await Repo.ad.closeIds(adIdsToDelete)

                // remove drivers from ad_driver
                driverIds = await Repo.adDriver.driverIdsFrom(adIdsToDelete)
                await Repo.adDriver.destroyFromAdIds(adIdsToDelete, t)

                // set status null to ad owners, invalidate session
                adUserIds = await Repo.ad.userIdsFrom(adIdsToDelete)
                await Repo.user.statusIdle(adUserIds, t)
                SessionService.invalidateById(adUserIds)
            })

            CacheService.adsActive = _omit(CacheService.adsActive, adIdsToDelete)

            // notify drivers & ad owners
            const userIds = await Repo.driver.driverIdsToUserIds(driverIds)
            userIds.push(...adUserIds)
            await MobileService.send(userIds, null, null, 'ad_remove')

        } catch (e) {
            loge(e.message, {adIds})
        }

    }

}

module.exports = AdDriverService