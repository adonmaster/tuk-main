const mailer = require('nodemailer')
const GmailServiceResponse = require('./GmailServiceResponse')

/**
 * @type {{init(): void, sendRegistration(*): Promise<void>}}
 */
module.exports = {

    init() {
        this.transporter = mailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PWD
            }
        })
    },

    /**
     *
     * @param {string[]} emails
     * @param {string} subject
     * @param {string} text
     * @returns {Promise<exports|*>}
     */
    async send(emails, subject, text)
    {
        if (process.env.GMAIL_SANDBOX === 'true') {
            console.log(`GmailService | to: [${emails.join(' ')}]\nsubject: [${subject}]\ntext: [${text}]`)
            return new GmailServiceResponse(true, 'Executado em sandbox')
        }


        try {
            await this.transporter.sendMail({
                from: '"TukTuk Brasil" <contato@tuktukapp.com.br>',
                to: emails.join(' '),
                subject,
                text
            })
            return new GmailServiceResponse(true, 'Executado com sucesso')
        } catch (e) {
            return new GmailServiceResponse(false, `GmailService@ ${e.message}`)
        }
    },

    /**
     *
     * @param {string} email
     * @param {string} code
     * @returns {Promise<exports|*>}
     */
    async sendRegistration(email, code) {
        return await this.send([email], "Registro TukTuk 🛺", `Olá!\nSeu código de registro é: ${code}`)
    }

}