const Repo = require('../repos/Repo')
const LightCache = require('../helpers/LightCache')
const _isArray = require('lodash/isArray')

const SessionService = {

    __cache: new LightCache(),

    async userFrom(token) {
        return this.__cache.retrieve(token, () => Repo.user.findByToken(token))
    },

    invalidateByToken(token) {
        return this.__cache.invalidate(token)
    },

    /**
     *
     * @param {int|int[]} id
     */
    invalidateById(id) {
        const list = _isArray(id) ? id : [id]
        this.__cache.filter((_, u) => u && !list.includes(u.id))
    }

}

module.exports = SessionService