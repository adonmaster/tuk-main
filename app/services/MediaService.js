const axios = require('axios')
const Http = require("../helpers/Http")

const MediaService = {

    /**
     *
     * @param {string[]} uids
     * @param {string|null} owner
     * @returns {Promise<void>}
     */
    async owner(uids, owner) {
        try {
            const url = process.env.MEDIA_URL_OWNER

            console.log(`MediaService: attempting ${url}`)
            return await axios.post(url, {uids, owner})
        } catch (e) {
            throw new Error(Http.translateAxiosError(e))
        }
    },

    __stashUids: [],

    /**
     *
     * @param {string} newOwner
     * @param {Object} newObj
     * @param {Object|null} oldObj
     * @param {string[]} fields
     */
    stashUidChanges(newOwner, newObj, oldObj, fields) {
        const old = oldObj || {}
        fields.forEach(field => {
            const vold = old[field]
            const vnew = newObj[field]
            if (vold && vold !== vnew) {
                const index = this.__stashUids.findIndex(({uid}) => uid===vold)
                const obj = {uid: vold, owner: null}
                if (~index) {
                    this.__stashUids[index] = obj
                } else {
                    this.__stashUids.push(obj)
                }
            }
            if (vnew && vnew !== vold) {
                const index = this.__stashUids.findIndex(({uid}) => uid===vnew)
                const obj = {uid: vnew, owner: newOwner}
                if (~index) {
                    this.__stashUids[index] = obj
                } else {
                    this.__stashUids.push(obj)
                }
            }
        })
    },

    stashUidInvalidate() {
        this.__stashUids = []
    },

    async stashUidFlush()
    {
        // categorize array
        const r = []
        this.__stashUids.forEach(({uid, owner}) => {
            const index = r.findIndex(({owner:o}) => o===owner)
            if (~index) {
                r[index].uids.push(uid)
            } else {
                r.push({owner, uids: [uid]})
            }
        })

        console.log(r)
        // sending requests
        for (const item of r) {
            await this.owner(item.uids, item.owner)
        }
        console.log('media service flushed')

        this.stashUidInvalidate()
    }
}

module.exports = MediaService