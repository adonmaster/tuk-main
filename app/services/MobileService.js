const firebase = require('firebase-admin')
const Repo = require("../repos/Repo");
const Categorizer = require("../helpers/Categorizer");
const _ = require('lodash')
const Str = require("../helpers/Str");

const MobileService = {

    async init() {
        const serviceAccount = require("../../res/tuktuk-339403-firebase-adminsdk-hx5us-3269b48f84");
        firebase.initializeApp({ credential: firebase.credential.cert(serviceAccount) });
    },

    /**
     *
     * @param {int[]|User[]} users
     * @param {string|null} title
     * @param {string|null} body
     * @param {string|null} payload
     */
    async send(users, title, body, payload)
    {
        console.log(`-----------> MobileService@send ${users.join(';')} | ${payload}`)

        if (users.length === 0) return

        let models = users
        if (typeof users[0] === 'number') {
            models = await Repo.user.idsToMobileToken(users);
        }
        const categorizerResult = Categorizer.process(models, ({mobile_service:s}) => s)

        try {

            const fcm = categorizerResult.items('fcm')
            if (fcm.length) {
                await this.__sendFcm(fcm.map(({mobile_token:t}) => t), title, body, payload)
            }

            const apn = categorizerResult.items('apn')
            if (apn.length) {
                await this.__sendApn(apn.map(({mobile_token:t}) => t), title, body, payload)
            }

        } catch (e) {
            loge(e.message, users)

            //bubble
            throw e
        }
    },

    isSandbox: process.env.MOBILE_SANDBOX === 'true',

    /**
     *
     * @param {string[]} tokens
     * @param {string|null} title
     * @param {string|null} body
     * @param {string|null} payload
     * @returns {Promise<void>}
     * @private
     */
    async __sendFcm(tokens, title, body, payload)
    {
        console.log(`${this.isSandbox ? '[SANDBOX]' : ''} MobileService: Sending fcm to ${
            tokens.map(s => Str.ellipsizeStart(s, 6)).join(', ')
        }`)

        if (this.isSandbox) return

        await firebase.messaging().sendMulticast({
            tokens, data: _.omitBy({ title, body, payload }, _.isNil)
        })
    },

    async __sendApn(tokens, title, body, payload) {
        console.log(`${this.isSandbox ? '[SANDBOX]' : ''} MobileService: Sending apn to ${
            tokens.map(s => Str.ellipsizeStart(s, 6)).join(', ')
        }`)

        // if (this.isSandbox) return
    }

}

module.exports = MobileService