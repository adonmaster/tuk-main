const cron = require('node-cron')

const AdDriverService = require('./AdDriverService')

const CronService = {

    init() {

        cron.schedule('* * * * *', AdDriverService.cronCleanUp, {})
        cron.schedule('* * * * *', () => AdDriverService.notify(), {})

    }

}

module.exports = CronService