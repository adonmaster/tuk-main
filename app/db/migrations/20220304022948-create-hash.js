'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('hashes', {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      key: { type: Sequelize.STRING(20), allowNull: false },
      hash: { type: Sequelize.STRING, allowNull: false },
      param_i: { type: Sequelize.INTEGER }

    }).then(() => {
      queryInterface.addIndex('hashes', ['key'], {token: {name: 'hashes_key', unique: false}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('hashes', ['hashes_key'])
    await queryInterface.dropTable('hashes');
  }
};