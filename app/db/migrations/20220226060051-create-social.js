'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('social', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      user_id: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: false },
      like: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: false },
      dislike: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: false },
      comment_positive: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: false },
      comment_negative: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: false },

    }).then(()=>{

      queryInterface.addIndex('social', ['user_id'], {token: {name: 'social_user_id', unique: false}})

    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('social', ['social_user_id'])
    await queryInterface.dropTable('social');
  }
};