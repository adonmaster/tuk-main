'use strict';
module.exports = {

  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false, autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER
      },
      is_admin: {type: Sequelize.BOOLEAN, defaultValue: false},
      email: {type: Sequelize.STRING, unique: true, isNull: false},
      name: {type: Sequelize.STRING, isNull: false},
      password: {type: Sequelize.STRING(20), isNull: false},
      token: {type: Sequelize.STRING(60), isNull: false},
      mobile_token: {type: Sequelize.STRING(120)},
      mobile_service: {type: Sequelize.STRING(10)},
      status: { type: Sequelize.STRING(15) },
      avatar_thumb: Sequelize.STRING,
      avatar_full: Sequelize.STRING,
      avatar_uid: Sequelize.STRING,
      created_at: {allowNull: false, type: Sequelize.DATE},
      updated_at: {allowNull: false, type: Sequelize.DATE}
    }).then(()=>{
      queryInterface.addIndex('users', ['token'], {token: {name: 'users_token', unique: false}})
      queryInterface.addIndex('users', ['status'], {token: {name: 'users_status', unique: false}})
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('users', ['users_token', 'users_status'])
    await queryInterface.dropTable('users');
  }

};