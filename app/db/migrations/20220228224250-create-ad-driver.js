'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ad_driver', {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      ad_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_id: { type: Sequelize.INTEGER, allowNull: false }

    }).then(()=>{
      queryInterface.addIndex('ad_driver', ['ad_id'], {token: {name: 'ad_driver_ad_id', unique: false}})
      queryInterface.addIndex('ad_driver', ['driver_id'], {token: {name: 'ad_driver_driver_id', unique: false}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('ad_driver', ['ad_driver_ad_id', 'ad_driver_driver_id'])
    await queryInterface.dropTable('ad_driver');
  }
};