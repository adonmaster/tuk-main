'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('interests', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      subject_name: { type: Sequelize.STRING(20) },
      subject_id: { type: Sequelize.INTEGER },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(()=>{
      queryInterface.addIndex('interests', ['subject_name', 'subject_id'], {token: {name: 'interests_subject', unique: true}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('interests', ['interests_subject'])
    await queryInterface.dropTable('interests');
  }
};