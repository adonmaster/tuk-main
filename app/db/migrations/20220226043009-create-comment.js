'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      from_user_id: {
        type: Sequelize.INTEGER, allowNull: false
      },
      to_user_id: {
        type: Sequelize.INTEGER, allowNull: false
      },
      body: {
        type: Sequelize.TEXT, allowNull: false
      },
      is_positive: {
        type: Sequelize.BOOLEAN, defaultValue: true
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(()=>{

      queryInterface.addIndex('comments', ['from_user_id'], {token: {name: 'comments_from_user_id', unique: false}})
      queryInterface.addIndex('comments', ['to_user_id'], {token: {name: 'comments_to_user_id', unique: false}})
      queryInterface.addIndex('comments', ['is_positive'], {token: {name: 'comments_is_positive', unique: false}})

    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('comments', ['comments_from_user_id', 'comments_to_user_id', 'comments_is_positive'])
    await queryInterface.dropTable('comments');
  }
};