'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.createTable('ad_proposal', {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      ad_id: { type: Sequelize.INTEGER, allowNull: false },
      ad_user_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_user_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_name_first: { type: Sequelize.STRING, allowNull: false },
      driver_name_last: { type: Sequelize.STRING, allowNull: false },
      driver_pos_lat: { type: Sequelize.DOUBLE, allowNull: false },
      driver_pos_lng: { type: Sequelize.DOUBLE, allowNull: false },
      driver_avatar_thumb: { type: Sequelize.STRING },
      driver_car: { type: Sequelize.STRING, allowNull: false },
      price: { type: Sequelize.DECIMAL(10,2), allowNull: false },
      arrival: { type: Sequelize.INTEGER, allowNull: false },

      created_at: { allowNull: false, type: Sequelize.DATE },
      updated_at: { allowNull: false, type: Sequelize.DATE }

    }).then(()=>{
      queryInterface.addIndex('ad_proposal', ['ad_id'], {token: {name: 'ad_proposal_ad_id', unique: false}})
      queryInterface.addIndex('ad_proposal', ['ad_user_id'], {token: {name: 'ad_proposal_ad_user_id', unique: false}})
      queryInterface.addIndex('ad_proposal', ['driver_id'], {token: {name: 'ad_proposal_driver_id', unique: false}})
      queryInterface.addIndex('ad_proposal', ['driver_user_id'], {token: {name: 'ad_proposal_driver_user_id', unique: false}})
      queryInterface.addIndex('ad_proposal', ['price'], {token: {name: 'ad_proposal_price', unique: false}})
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('ad_proposal', [
        'ad_proposal_ad_id', 'ad_proposal_ad_user_id', 'ad_proposal_driver_id', 'ad_proposal_driver_user_id', 'ad_proposal_price'
    ])
    await queryInterface.dropTable('ad_proposal');
  }

};