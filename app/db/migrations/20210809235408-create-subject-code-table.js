'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('subject_code', {
      id: {
        allowNull: false, autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER
      },
      subject: {type: Sequelize.STRING(255), allowNull: false, unique: true},
      code: {type: Sequelize.STRING(50), allowNull: false},
      expired_at: {type: Sequelize.DATE, allowNull: false}
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('subject_code')
  }
};
