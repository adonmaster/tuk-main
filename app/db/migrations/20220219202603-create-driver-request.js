'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('driver_request', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: { type: Sequelize.INTEGER, allowNull: false },

      avatar: { type: Sequelize.STRING },
      avatar_thumb: { type: Sequelize.STRING },
      avatar_uid: { type: Sequelize.STRING },

      name_first: { type: Sequelize.STRING },
      name_last: { type: Sequelize.STRING },

      gender: { type: Sequelize.STRING(10) },
      birth: { type: Sequelize.DATE },
      cpf: { type: Sequelize.STRING(20) },

      ref_city: { type: Sequelize.STRING },
      ref_uf: { type: Sequelize.STRING },

      license_no: { type: Sequelize.STRING(20) },
      license_valid_until: { type: Sequelize.DATE },
      license_img: { type: Sequelize.STRING },
      license_img_uid: { type: Sequelize.STRING },

      legal_type: { type: Sequelize.STRING(20) },
      legal_img: { type: Sequelize.STRING },
      legal_img_uid: { type: Sequelize.STRING },

      car_brand: { type: Sequelize.STRING },
      car_model: { type: Sequelize.STRING },
      car_color: { type: Sequelize.STRING },
      car_plate: { type: Sequelize.STRING },
      car_img: { type: Sequelize.STRING },
      car_img_uid: { type: Sequelize.STRING },
      car_doc_img: { type: Sequelize.STRING },
      car_doc_img_uid: { type: Sequelize.STRING },

      closed_at: { type: Sequelize.DATE },
      updated_at: { allowNull: false, type: Sequelize.DATE },
      created_at: { allowNull: false, type: Sequelize.DATE }

    }).then(()=>{
      queryInterface.addIndex('driver_request', ['user_id'], {token: {name: 'driver_request_user_id', unique: false}})
      queryInterface.addIndex('driver_request', ['cpf'], {token: {name: 'driver_request_cpf', unique: false}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('driver_request', 'driver_request_user_id', 'driver_request_cpf')
    await queryInterface.dropTable('driver_request');
  }
};