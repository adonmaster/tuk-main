'use strict';
module.exports = {

  async up(queryInterface, Sequelize) {

    await queryInterface.createTable('contracts', {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      ad_id: { type: Sequelize.INTEGER, allowNull: false },
      ad_user_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_id: { type: Sequelize.INTEGER, allowNull: false },
      driver_user_id: { type: Sequelize.INTEGER, allowNull: false },
      price: { type: Sequelize.DOUBLE, allowNull: false },

      run_ini: { type: Sequelize.DATE },
      run_end: { type: Sequelize.DATE },

      created_at: { allowNull: false, type: Sequelize.DATE },
      updated_at: { allowNull: false, type: Sequelize.DATE }

    }).then(()=>{
      queryInterface.addIndex('contracts', ['ad_id'], {token: {name: 'contracts_ad_id', unique: false}})
      queryInterface.addIndex('contracts', ['driver_id'], {token: {name: 'contracts_driver_id', unique: false}})
      queryInterface.addIndex('contracts', ['run_ini'], {token: {name: 'contracts_run_ini', unique: false}})
      queryInterface.addIndex('contracts', ['run_end'], {token: {name: 'contracts_run_end', unique: false}})
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('contracts', ['ad_id', 'driver_id', 'run_ini', 'run_end'])
    await queryInterface.dropTable('contracts');
  }

};