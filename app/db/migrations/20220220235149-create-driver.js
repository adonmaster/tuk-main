'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('drivers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      user_id: {type: Sequelize.INTEGER, allowNull: false},

      name_first: {type: Sequelize.STRING, allowNull: false },
      name_last: {type: Sequelize.STRING, allowNull: false },
      cpf: {type: Sequelize.STRING(20), allowNull: false},

      ref_city: {type: Sequelize.STRING, allowNull: false },
      ref_uf: {type: Sequelize.STRING, allowNull: false },

      avatar: {type: Sequelize.STRING },
      avatar_thumb: {type: Sequelize.STRING },
      avatar_uid: {type: Sequelize.STRING },

      pos_lat: {type: Sequelize.DOUBLE, defaultValue: 0 },
      pos_lng: {type: Sequelize.DOUBLE, defaultValue: 0 },

      car_id: { type: Sequelize.INTEGER, allowNull: false },

      active: { type: Sequelize.BOOLEAN },
      online: { type: Sequelize.BOOLEAN, defaultValue: 0 },
      points: { type: Sequelize.INTEGER, defaultValue: 0 },

      updated_at: { type: Sequelize.DATE },
      created_at: { type: Sequelize.DATE }

    }).then(()=>{
      queryInterface.addIndex('drivers', ['user_id'], {token: {name: 'drivers_user_id', unique: false}})
      queryInterface.addIndex('drivers', ['cpf'], {token: {name: 'drivers_cpf', unique: false}})
      queryInterface.addIndex('drivers', ['pos_lat'], {token: {name: 'drivers_pos_lat', unique: false}})
      queryInterface.addIndex('drivers', ['pos_lng'], {token: {name: 'drivers_pos_lng', unique: false}})
      queryInterface.addIndex('drivers', ['active'], {token: {name: 'drivers_active', unique: false}})
      queryInterface.addIndex('drivers', ['online'], {token: {name: 'drivers_online', unique: false}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('drivers', [
      'drivers_user_id', 'drivers_pos_lat', 'drivers_pos_lng',
      'drivers_active', 'drivers_online', 'drivers_cpf'
    ])
    await queryInterface.dropTable('drivers');
  }
};