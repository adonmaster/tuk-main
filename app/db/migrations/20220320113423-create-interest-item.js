'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('interest_item', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      subject_name: { type: Sequelize.STRING(20) },
      subject_id: { type: Sequelize.INTEGER },
      user_id: { type: Sequelize.INTEGER },
      pinned: { type: Sequelize.BOOLEAN, defaultValue: false },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(()=>{
      queryInterface.addIndex('interest_item', ['subject_name', 'subject_id'], {token: {name: 'interest_item_subject', unique: false}})
      queryInterface.addIndex('interest_item', ['user_id'], {token: {name: 'interest_item_user_id'}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('interest_item', ['interest_item_subject', 'interest_item_user_id'])
    await queryInterface.dropTable('interest_item');
  }
};