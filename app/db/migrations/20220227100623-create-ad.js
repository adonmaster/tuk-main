'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ads', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      user_id: { type: Sequelize.INTEGER, allowNull: false },
      user_name: { type: Sequelize.STRING, allowNull: false },

      origin_lat: { type: Sequelize.DOUBLE, allowNull: false },
      origin_lng: { type: Sequelize.DOUBLE, allowNull: false },
      origin_s: { type: Sequelize.STRING, allowNull: false },

      destination_lat: { type: Sequelize.DOUBLE, allowNull: false },
      destination_lng: { type: Sequelize.DOUBLE, allowNull: false },
      destination_s: { type: Sequelize.STRING, allowNull: false },

      route: { type: Sequelize.TEXT, allowNull: false },

      duration: { type: Sequelize.INTEGER, allowNull: false },
      duration_s: { type: Sequelize.STRING, allowNull: false },

      distance: { type: Sequelize.INTEGER, allowNull: false },
      distance_s: { type: Sequelize.STRING, allowNull: false },

      price: { type: Sequelize.DECIMAL(10,2), allowNull: false },

      closed_at: { type: Sequelize.DATE },

      created_at: { allowNull: false, type: Sequelize.DATE }
    }).then(()=>{
      queryInterface.addIndex('ads', ['user_id'], {token: {name: 'ads_user_id', unique: false}})
      queryInterface.addIndex('ads', ['origin_lat'], {token: {name: 'ads_origin_lat', unique: false}})
      queryInterface.addIndex('ads', ['origin_lng'], {token: {name: 'ads_origin_lng', unique: false}})
      queryInterface.addIndex('ads', ['destination_lat'], {token: {name: 'ads_destination_lat', unique: false}})
      queryInterface.addIndex('ads', ['destination_lng'], {token: {name: 'ads_destination_lng', unique: false}})
      queryInterface.addIndex('ads', ['closed_at'], {token: {name: 'ads_closed_at', unique: false}})
      queryInterface.addIndex('ads', ['created_at'], {token: {name: 'ads_created_at', unique: false}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('ads', [
      'ads_user_id',
      'ads_origin_lat',
      'ads_origin_lng',
      'ads_destination_lat',
      'ads_destination_lng',
      'ads_closed_at',
      'ads_created_at',
    ])
    await queryInterface.dropTable('ads');
  }
};