'use strict';
module.exports = {

  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('driver_car', {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      driver_id: { type: Sequelize.INTEGER, notNull: false },

      plate: { type: Sequelize.STRING(20), notNull: false },
      brand: { type: Sequelize.STRING },
      model: { type: Sequelize.STRING },
      color: { type: Sequelize.STRING },
      img: { type: Sequelize.STRING },
      img_uid: { type: Sequelize.STRING },
      created_at: { allowNull: false, type: Sequelize.DATE }

    }).then(()=>{
      queryInterface.addIndex('driver_car', ['driver_id'], {token: {name: 'driver_driver_id', unique: false}})
      queryInterface.addIndex('driver_car', ['plate'], {token: {name: 'driver_car_plate', unique: false}})
    });

  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeIndex('driver_car', ['driver_car_plate', 'driver_driver_id'])
    await queryInterface.dropTable('driver_car');
  }
};