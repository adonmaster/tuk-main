const { AdProposal } = require('../models')

const AdProposalRepo = {

    async getFromAd(ad_id) {
        return await AdProposal.findAll({where: {ad_id}, order: [['price', 'asc']]})
    },

    /**
     *
     * @param ad_id
     * @param ad_user_id
     * @param driver_id
     * @param driver_user_id
     * @param driver_name_first
     * @param driver_name_last
     * @param driver_pos_lat
     * @param driver_pos_lng
     * @param driver_avatar_thumb
     * @param driver_car
     * @param price
     * @param arrival
     * @returns {Promise<AdProposal>}
     */
    async save(
        ad_id, ad_user_id, driver_id, driver_user_id, driver_name_first, driver_name_last, driver_pos_lat, driver_pos_lng,
        driver_avatar_thumb, driver_car, price, arrival
    ) {
        const attributes = {ad_id, ad_user_id, driver_id, driver_user_id, driver_name_first, driver_name_last, driver_pos_lat, driver_pos_lng,
            driver_avatar_thumb, driver_car, price, arrival}

        let model = await AdProposal.findOne({attributes: ['id'], where: {driver_id}})
        if (! model) return AdProposal.create(attributes)

        await model.update(attributes)
        await model.reload()
        return model
    },

    async update(id, price, arrival) {
        return await AdProposal.update({price, arrival}, {where: {id}})
    }
}

module.exports = AdProposalRepo