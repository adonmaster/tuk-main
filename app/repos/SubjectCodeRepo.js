const { SubjectCode } = require('../models')
const { Op } = require('sequelize')
const Str = require("../helpers/Str");
const moment = require('moment')

const SubjectCodeRepo = {

    async find(subject) {
        return await SubjectCode.findOne({where: {subject}})
    },

    /**
     *
     * @param subject
     * @returns {Promise<SubjectCode>}
     */
    async save(subject)
    {
        const code = Str.padLeftFixed(Math.random() * 999_999, 6, '0')
        const expired_at = moment().add(2, 'hours').toDate()

        let model = await this.find(subject)
        if ( ! model) {
            return await SubjectCode.create({subject, code, expired_at})
        }

        if (model.expired_at < new Date) {
            model.expired_at = expired_at
            model.code = code
            await model.save()
        }

        return model
    },

    async check(subject, code) {
        return await SubjectCode.count({
            where: {
                subject, code,
                expired_at: {[Op.gte]: new Date}
            }
        }) > 0
    }
}

module.exports = SubjectCodeRepo