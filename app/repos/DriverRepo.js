const moment = require('moment')
const { Driver, DriverCar, Social, sequelize } = require('../models')
const { Op } = require('sequelize')
const ModelNotFoundError = require('../errors/ModelNotFoundError')
const MediaService = require("../services/MediaService");

const DriverRepo = {

    /**
     *
     * @param {int} user_id
     * @returns {Promise<Driver|null>}
     */
    async findByUserId(user_id) {
        return Driver.findOne({where: {user_id}})
    },

    /**
     *
     * @param {int} user_id
     * @param {string[]|null} attributes
     * @returns {Promise<Driver>}
     * @throws
     */
    async findByUserOrFail(user_id, attributes=null)
    {
        let options = {where: {user_id}}
        if (attributes) options['attributes'] = attributes

        const model = await Driver.findOne(options)

        if (model) return model
        throw new ModelNotFoundError('Motorista não encontrado')
    },

    async userIdToDriverIdOrFail(user_id) {
        const model = await this.findByUserOrFail(user_id, ['id', 'user_id'])
        return model.id
    },

    async driverIdsToUserIds(driver_id_list) {
        const drivers = await Driver.findAll({
            attributes: ['user_id'],
            where: {id: driver_id_list}
        })
        return drivers.map(({user_id}) => user_id)
    },

    /**
     *
     * @param {DriverRequest} driverRequestModel
     * @param transaction
     * @returns {Promise<Driver>}
     */
    async saveFrom(driverRequestModel, transaction=null)
    {
        const params = {
            user_id: driverRequestModel.user_id,
            name_first: driverRequestModel.name_first,
            name_last: driverRequestModel.name_last,
            cpf: driverRequestModel.cpf,
            avatar: driverRequestModel.avatar,
            avatar_thumb: driverRequestModel.avatar_thumb,
            avatar_uid: driverRequestModel.avatar_uid,
            ref_city: driverRequestModel.ref_city,
            ref_uf: driverRequestModel.ref_uf,
            car_id: 0,
            active: true,
            online: false
        }

        let oldParams
        let model = await Driver.findOne({
            where: {cpf: driverRequestModel.cpf},
            transaction
        })
        if ( ! model) {
            oldParams = null
            model = await Driver.create(params, {transaction})
        } else {
            oldParams = model.toJSON()
            await model.update(params, {transaction})
        }

        MediaService.stashUidChanges(`driver:${model.id}`, params, oldParams, ['avatar_uid'])

        return model
    },

    /**
     *
     * @param {number} driverId
     * @param {DriverRequest} request
     * @param transaction
     * @returns {Promise<DriverCar>}
     */
    async saveCarFrom(driverId, request, transaction=null)
    {
        const params = {
            driver_id: driverId,
            plate: request.car_plate,
            brand: request.car_brand,
            model: request.car_model,
            color: request.car_color,
            img: request.car_img,
            img_uid: request.car_img_uid
        }

        let model = await DriverCar.findOne({where: {plate: request.car_plate}})
        let oldParams
        if ( ! model) {

            oldParams = null
            model = await DriverCar.create(params, {transaction})

        } else {

            if (model.driver_id != driverId) throw new Error('A placa deste veículo já está vinculado à outro motorista')

            oldParams = model.toJSON()
            await model.update(params, {transaction})
        }

        MediaService.stashUidChanges(`driver-car:${model.id}`, params, oldParams, ['car_img_uid'])

        return model
    },

    /**
     *
     * @param driver_id
     * @param car_id
     * @param {boolean} transaction
     * @returns {Promise<void>}
     */
    async updateDefCar(driver_id, car_id, transaction) {
        await Driver.update({car_id}, {
            where: {id: driver_id},
            transaction
        })
    },
    /**
     *
     * @param {int} user_id
     * @param {number} pos_lat
     * @param {number} pos_lng
     */
    async updatePosition(user_id, pos_lat, pos_lng) {
        await Driver.update({pos_lat, pos_lng}, {where: {user_id}})
    },
    /**
     *
     * @param {int} user_id
     * @param {boolean} option
     */
    async updateOnline(user_id, option) {
        await Driver.update({online: option}, {where: {user_id}})
    },

    /**
     *
     * @param {number} lat
     * @param {number} lng
     * @param {number} factor
     * @param {int} limit
     * @param {int[]} excluding
     * @returns {Promise<int[]>}
     */
    async getActiveIdsOrderByPts(lat, lng, factor, limit, excluding=[])
    {
        const list = await Driver.scope('online').findAll({
            attributes: [ 'id' ],
            where: {
                id: {[Op.notIn]: excluding},
                pos_lat: {[Op.between]: [lat - factor, lat + factor]},
                pos_lng: {[Op.between]: [lng - factor, lng + factor]}
            },
            order: [['points', 'desc']],
            limit
        })
        return list.map(({id}) => id);
    }
}

module.exports = DriverRepo