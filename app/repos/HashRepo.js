const { Hash } = require('../models')
const Crypt = require("../helpers/Crypt");


const HashRepo = {

    /**
     *
     * @param key
     * @returns {Promise<Hash>}
     */
    async find(key) {
        return Hash.findOne({where: {key}})
    },

    /**
     *
     * @param {...string} key
     * @returns {Promise<Hash>}
     */
    async __invalidate(...key)
    {
        const skey = key.join('.')
        const hash = Crypt.random(32)

        const cont = await Hash.count({where: {key: skey}})
        if (cont) {
            await Hash.update({hash}, {where: {key: skey}})
        } else {
            await Hash.create({key: skey, hash})
        }
    },

    /**
     *
     * @param {int[]} driver_ids
     */
    async invalidateDriversAdList(driver_ids) {
        for (let id of driver_ids) {
            this.__invalidate('driver_ad_list', id+'')
        }
    },

    async invalidateAdProposal(ad_id) {
        await this.__invalidate('ad_proposal', ad_id)
    }
}

module.exports = HashRepo