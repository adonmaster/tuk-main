const ModelNotFoundError = require("../errors/ModelNotFoundError");
const {Social} = require('../models')

const SocialRepo = {

    /**
     *
     * @param user_id
     * @returns {Promise<[Social, boolean]>}
     */
    async findOrCreate(user_id) {
        return await Social.findOrCreate({ where: { user_id },
            defaults: {
                like:0,
                dislike:0,
                comment_positive:0,
                comment_negative:0,
            }
        })
    }
}

module.exports = SocialRepo