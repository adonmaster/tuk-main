const MediaService = require("../services/MediaService");
const { DriverCar } = require('../models')
const ModelNotFoundError = require('../errors/ModelNotFoundError')


const DriverCarRepo = {

    async getFrom(driver_id) {
        return await DriverCar.findAll({where: {driver_id}})
    },

    /**
     *
     * @param plate
     * @returns {Promise<DriverCar>}
     */
    async findByPlate(plate) {
        return await DriverCar.findOne({where: {plate}})
    },

    async find(id) {
        return await DriverCar.findByPk(id)
    },

    /**
     *
     * @param id
     * @returns {Promise<DriverCar>}
     */
    async findOrFail(id) {
        const model = await this.find(id)
        if (! model) ModelNotFoundError.t('Veículo não encontrado')
        return model
    },

    async save(driver_id, plate, brand, model, color, img, img_uid)
    {
        const params = {driver_id, plate, brand, model, color, img, img_uid}

        let car = await this.findByPlate(plate)
        let oldParams
        if ( ! car)
        {
            oldParams = null
            car = await DriverCar.create(params)
        }
        else
        {
            if (car.driver_id !== driver_id) throw new Error('Pela placa este carro já é de um outro motorista')

            oldParams = car.toJSON()
            await car.update(params)
        }

        MediaService.stashUidChanges(`driver:${driver_id}`, params, oldParams, ['img_uid'])

        return car
    },

    async destroy(carId) {
        await DriverCar.destroy({where: {id: carId}})
    }
}

module.exports = DriverCarRepo