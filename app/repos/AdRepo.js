const { Ad, User } = require('../models')
const { Op } = require('sequelize')
const ModelNotFoundError = require('../errors/ModelNotFoundError')
const Dt = require("../helpers/Dt");

const AdRepo = {

    /**
     *
     * @param {int} id
     * @param {string[]|null} attributes
     * @returns {Promise<Ad>}
     */
    async findOrFail(id, attributes=null) {
        const options = {}
        if (attributes) options.attributes = attributes

        const model = await Ad.findByPk(id, options)
        if (model) return model

        ModelNotFoundError.t(`Anúncio não encontrado [${id}]`)
    },

    async findActive(user_id, attributes=null, exclude=null) {
        return await Ad.scope('active').findOne({where: {user_id}, attributes, exclude})
    },

    /**
     *
     * @param {int|int[]|null} ids
     * @param {boolean} loadSimpleUser
     * @returns {Promise<Model[]>}
     */
    async getActiveWithoutRoute(ids=null, loadSimpleUser=false)
    {
        const options = {}
        if (ids) options.where = { id: ids }
        options.attributes = { exclude: ['route'] }
        if (loadSimpleUser) {
            options.include = [{model: User, as: 'user', attributes: ['id', 'name', 'avatar_thumb', 'avatar_full']}]
        }

        return await Ad.scope('active').findAll(options)
    },

    async existsFromUserId(ad_id, user_id) {
        return await Ad.count({where: {id: ad_id, user_id}}) > 0
    },

    /**
     *
     * @param {int} user_id
     * @param {string} user_name
     * @param {double} origin_lat
     * @param {double} origin_lng
     * @param {string} origin_s
     * @param {double} destination_lat
     * @param {double} destination_lng
     * @param {string} destination_s
     * @param {string} route
     * @param {int} duration
     * @param {string} duration_s
     * @param {int} distance
     * @param {string} distance_s
     * @param {double} price
     * @returns {Promise<Ad>}
     */
    async create(
        user_id, user_name, origin_lat, origin_lng, origin_s, destination_lat, destination_lng, destination_s, route,
        duration, duration_s, distance, distance_s, price
    )
    {
        return await Ad.create({
            user_id, user_name, origin_lat, origin_lng, origin_s, destination_lat, destination_lng, destination_s, route,
            duration, duration_s, distance, distance_s, price
        })
    },
    /**
     *
     * @param ad_id_list
     * @param transaction
     * @returns {Promise<void>}
     */
    async closeIds(ad_id_list, transaction=null)
    {
        await Ad.update({ closed_at: Dt.now()}, {
            where: {
                id: ad_id_list,
                closed_at: {[Op.is]: null}
            },
            transaction
        })
    },
    /**
     *
     * @param {int[]} adIds
     * @returns {Promise<int[]>}
     */
    async userIdsFrom(adIds) {
        const models = await Ad.findAll({where: {id: adIds}, attributes: ['id', 'user_id']})
        return models.map(({user_id}) => user_id)
    }
}

module.exports = AdRepo