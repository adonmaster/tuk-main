const { Comment, User } = require('../models')

const CommentRepo = {

    async getAllWithSafeUser(to) {
        return await Comment.findAll({
            where: { to_user_id: to },
            include: [
                {model: User, as: 'from', attributes: ['id', 'name', 'email', 'avatar_full', 'avatar_thumb']}
            ],
            limit: 200,
            order: [['id', 'DESC']]
        })
    },

    /**
     *
     * @param from_user_id
     * @param to_user_id
     * @param body
     * @param is_positive
     * @returns {Promise<Comment>}
     */
    async create(from_user_id, to_user_id, body, is_positive) {
        return await Comment.create({from_user_id, to_user_id, body, is_positive})
    },

    async destroy(id) {
        await Comment.destroy({where: {id}})
    }
}

module.exports = CommentRepo