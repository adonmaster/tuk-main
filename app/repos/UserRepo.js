const { User, sequelize } = require('../models')
const { Op } = require('sequelize')
const Crypt = require('../helpers/Crypt')
const MediaService = require("../services/MediaService");
const ModelNotFoundError = require("../errors/ModelNotFoundError");

const UserRepo = {

    /**
     *
     * @param id
     * @returns {Promise<Model>}
     */
    async findById(id) {
        return await User.findByPk(id)
    },

    async findSafe(id) {
        return await User.scope('safe').findByPk(id)
    },

    async findByToken(token) {
        return await User.findOne({ where: {token}})
    },

    /**
     *
     * @param id
     * @param attributes
     * @returns {Promise<User>}
     */
    async findGraphOrFail(id, attributes=['id'])
    {
        const model = await User.findOne({ where: {id}, attributes })
        if (model) return model

        ModelNotFoundError.t('Usuário não encontrado')
    },

    /**
     *
     * @param where
     * @returns {Promise<User[]>}
     */
    get(where={}) {
        return User.findAll({where})
    },

    async save(email, name)
    {
        let user = await User.findOne({where: {email}})
        const token = await this.generateUniqueToken()
        if ( ! user) {
            const is_admin = ['adonio.silva@gmail.com', 'adonio@outlook.com'].includes(email)
            return await User.create({email, name, token, is_admin})
        }

        user.email = email
        user.name = name
        user.token = token
        await user.save()

        return await User.findByPk(user.id)
    },

    async generateUniqueToken() {
        let token
        do {
            token = await Crypt.random(20)
        }
        while (await User.count({where: {token}}) > 0)
        return token
    },

    async updateMobile(userId, mobile_service, mobile_token) {
        return await User.update({mobile_service, mobile_token}, {where: {id: userId}})
    },

    async updateNameAvatar(userId, name, avatar_uid, avatar_full, avatar_thumb)
    {
        const model = await User.findOne({attributes: ['id', 'avatar_uid']})
        if (! model) return

        let oldParams = model.toJSON()
        let params = {name, avatar_uid, avatar_full, avatar_thumb};

        await User.update(params, {where: {id: userId}})

        MediaService.stashUidChanges('user:' + userId, params, oldParams, ['avatar_uid'])
    },

    /**
     *
     * @param {int[]} userIds
     * @returns {[{id, mobile_service, mobile_token}]}
     */
    async idsToMobileToken(userIds) {
        const models = await User.findAll({
            attributes: ['id', 'mobile_service', 'mobile_token'],
            where: {id: userIds}
        })
        return models.map(m => m.toJSON())
    },

    async __status(user_id, s, transaction) {
        await User.update({status: s}, { where: {id: user_id}, transaction })
    },

    /**
     *
     * @param {int|int[]} user_id
     * @param transaction
     * @returns {Promise<void>}
     */
    async statusIdle(user_id, transaction=null) {
        await this.__status(user_id, null, transaction)
    },

    /**
     *
     * @param {int|int[]} user_id
     * @param transaction
     * @returns {Promise<void>}
     */
    async statusAd(user_id, transaction=null) {
        await this.__status(user_id, 'ad', transaction)
    },

    async statusContractDriver(user_id, transaction=null) {
        await this.__status(user_id, 'contract_driver', transaction)
    },

    async statusContractRunning(user_id, transaction=null) {
        await this.__status(user_id, 'contract_running', transaction)
    }

}

module.exports = UserRepo