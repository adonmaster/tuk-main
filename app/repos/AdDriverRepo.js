const { AdDriver } = require('../models')
const AdRepo = require('./AdRepo')

const AdDriverRepo = {

    /**
     *
     * @param {int} adId
     * @returns {Promise<int[]>}
     */
    async getFromAd_DriverIds(adId) {
        const models = await AdDriver.findAll({where: {ad_id: adId}})
        return models.map(({driver_id}) => driver_id)
    },


    async getFromDriver_Ads(driver_id, loadSimpleUser=false) {

        const models = await AdDriver.findAll({where: {driver_id}})
        const adIds = models.map(({ad_id})=>ad_id)

        return AdRepo.getActiveWithoutRoute(adIds, loadSimpleUser)
    },

    /**
     *
     * @param {int} adId
     * @param {int[]} driver_id_list
     * @returns {Promise<void>}
     */
    async registerDriverIds(adId, driver_id_list) {
        await AdDriver.bulkCreate(
            driver_id_list.map(driver_id => ({driver_id, ad_id: adId})),
            {validate: true}
            )
    },

    /**
     *
     * @param adIds
     * @returns {Promise<int[]>}
     */
    async driverIdsFrom(adIds=[]) {
        const models = await AdDriver.findAll({where: {ad_id: adIds}})
        return models.map(({driver_id}) => driver_id)
    },
    /**
     *
     * @param list
     * @param transaction
     * @returns {Promise<void>}
     */
    async destroyFromAdIds(list, transaction=null)
    {
        const options = {where: {ad_id: list}}
        if (transaction) options.transaction = transaction

        await AdDriver.destroy(options)
    }
}

module.exports = AdDriverRepo