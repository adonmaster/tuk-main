const ModelNotFoundError = require("../errors/ModelNotFoundError");
const { Contract } = require('../models')

const ContractRepo = {

    /**
     *
     * @param id
     * @returns {Promise<Contract>}
     */
    async findOrFail(id) {
        const model = await Contract.findByPk(id)
        if (model) return model

        ModelNotFoundError.t(`Contrato não encontrado (${id}).`)
    },

    /**
     *
     * @param {int} ad_user_id
     * @returns {Promise<Contract>}
     */
    async findPending(ad_user_id) {
        return Contract.scope('pending').findOne({where: {ad_user_id}})
    },

    async findRunningForDriver(driver_id) {
        return Contract.scope('running').findOne()
    },

    async store(ad_id, ad_user_id, driver_id, driver_user_id, price) {
        return Contract.create({ad_id, ad_user_id, driver_id, driver_user_id, price})
    },

    async start(id) {
        return Contract.update({run_ini: new Date()}, {where: {id}})
    },

    async finish(id) {
        return Contract.update({run_end: new Date()}, {where: {id}})
    }
}

module.exports = ContractRepo