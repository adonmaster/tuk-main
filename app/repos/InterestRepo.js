const {Interest, InterestItem} = require('../models')
const { Op } = require('sequelize')

const InterestRepo = {

    MINUTES_TIMEOUT: 3,

    /**
     *
     * @param {string} subject_name
     * @param {int} subject_id
     * @param {int} user_id
     * @param {boolean} pinned
     * @returns {Promise<void>}
     */
    async saveItem(subject_name, subject_id, user_id, pinned)
    {
        const params = {subject_name, subject_id, user_id};
        const model = await InterestItem.findOne({ attributes: ['id'], where: params })
        if ( ! model) {
            params.pinned = pinned
            await InterestItem.create(params)
        } else {
            model.changed('updated_at', true)
            await model.update({updated_at: new Date()})
        }
    },

    /**
     *
     * @param subject_name
     * @param subject_id
     * @returns {Promise<int[]>}
     */
    async saveHeaderAndFindInterestUserIdList(subject_name, subject_id)
    {
        let now = new Date();
        const [interest, created] = await Interest.findOrCreate({ where: {subject_name, subject_id} })
        if ( ! created) {
            interest.changed('updated_at', true)
            await interest.update({updated_at: now})
        }

        const someMinutesAgo = moment().subtract(this.MINUTES_TIMEOUT, 'minutes').toDate()
        const list = await InterestItem.findAll({
            attributes: ['user_id'],
            where: {
                subject_name, subject_id,
                [Op.or]: [
                    {pinned: true},
                    {updated_at: {[Op.gt]: someMinutesAgo}}
                ]
            }
        })

        return list.map(({user_id}) => user_id)
    }
}

module.exports = InterestRepo