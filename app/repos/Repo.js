const { sequelize } = require('../models')

const Repo = {

    user: require('./UserRepo'),

    driver: require('./DriverRepo'),
    driverCar: require('./DriverCarRepo'),
    driverRequest: require('./DriverRequestRepo'),

    comment: require('./CommentRepo'),
    social: require("./SocialRepo"),

    ad: require("./AdRepo"),
    adDriver: require('./AdDriverRepo'),
    adProposal: require('./AdProposalRepo'),

    contract: require('./ContractRepo'),

    hash: require('./HashRepo'),

    interest: require('./InterestRepo'),

    /**
     *
     * @param {Model} model
     * @param {string[]} associations
     */
    async reload(model, associations=[]) {
        await model.reload({include: associations})
    },


    async transaction(promise) {
        return await sequelize.transaction(promise)
    }
}

module.exports = Repo