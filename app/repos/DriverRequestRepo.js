const MediaService = require("../services/MediaService");
const { DriverRequest } = require('../models')
const ModelNotFoundError = require('../errors/ModelNotFoundError')

const DriverRequestRepo = {

    /**
     * @returns {Promise<DriverRequest>}
     */
    async save(user_id, avatar, avatar_thumb, avatar_uid, name_first, name_last, gender, birth, cpf, ref_city, ref_uf, license_no, license_valid_until, license_img, license_img_uid, legal_type, legal_img, legal_img_uid, car_brand, car_model, car_color, car_plate, car_img, car_img_uid, car_doc_img, car_doc_img_uid)
    {
        let model = await DriverRequest.findOne({where: {cpf}})

        let params = {user_id, avatar, avatar_thumb, avatar_uid, name_first, name_last, gender, birth, cpf, ref_city, ref_uf, license_no, license_valid_until, license_img, license_img_uid, legal_type, legal_img, legal_img_uid, car_brand, car_model, car_color, car_plate, car_img, car_img_uid, car_doc_img, car_doc_img_uid}
        params['closed_at'] = null

        let oldParams
        if (! model)
        {
            oldParams = null
            model = await DriverRequest.create(params)
        }
        else
        {
            oldParams = model.toJSON()
            await model.update(params)
        }

        MediaService.stashUidChanges(
            'driverRequest:' + model.id,
            params, oldParams,
            ['avatar_uid', 'license_img_uid', 'legal_img_uid', 'car_img_uid', 'car_doc_img_uid']
        )

        return model
    },

    /**
     *
     * @param driver_request_id
     * @returns {Promise<DriverRequest>}
     * @throws Error
     */
    async findOrError(driver_request_id) {
        const model = await DriverRequest.findOne({where: {id: driver_request_id}})
        if (model) return model
        throw new ModelNotFoundError('Request model not found')
    },

    async closeIt(driver_request_id, transaction) {
        await DriverRequest.update({close: true}, {
            where: {id: driver_request_id},
            transaction
        })
    }
}

module.exports = DriverRequestRepo