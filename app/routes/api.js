module.exports = router = require('express').Router()

// middlewares
const authMid = require('../mid/auth-mid')

// controllers
const ApiRegisterController = require('../controllers/ApiRegisterController')
const ApiGeoController = require('../controllers/ApiGeoController')
const ApiUserController = require('../controllers/ApiUserController')
const ApiDriverController = require('../controllers/ApiDriverController')
const ApiDriverCarController = require("../controllers/ApiDriverCarController");
const ApiCommentController = require("../controllers/ApiCommentController");
const ApiSocialController = require("../controllers/ApiSocialController");
const ApiAdController = require("../controllers/ApiAdController");
const ApiAdProposalController = require("../controllers/ApiAdProposalController");
const ApiHashController = require("../controllers/ApiHashController");
const ApiContractController = require("../controllers/ApiContractController");


router.get('/teste/:id', async (req, res) => {
    const { Sanitizer } = require('../helpers/Sanitizer')
    const data = res.validateAndSanitize({id: 'required'}, {})
    if (data === false) return

    const { id } = data

    console.log(id)

    res.send('id: ' + id)
})

router.get('/ping', async (req, res) => {
    try {
/*
        const userIds = [2]
        const body = `Jeruza precisa de sua ajuda!`
        const MobileService = require('../services/MobileService')
        await MobileService.send(userIds, 'Corrida!', body, 'ad')
        */
        const Repo = require('../repos/Repo')
        for (let i = 0; i<9; i++) {
            await Repo.ad.create(
                3, `${i} Carlito Diamante`, -16.76198, -49.2860333,
                'R. H-72A, 93 - Cidade Vera Cruz, Aparecida de Goiânia - GO, 74000-000, Brazil',
                -16.7410576, -49.2769892, 'Av. Rio Verde, 100 - Jardim Atlântico, Goiânia - GO, 74840-150, Brazil',
                [[{"lat":-16.762150000000002,"lng":-49.28607},{"lat":-16.76208,"lng":-49.28638}],[{"lat":-16.76208,"lng":-49.28638},{"lat":-16.762700000000002,"lng":-49.286500000000004},{"lat":-16.763080000000002,"lng":-49.28660000000001},{"lat":-16.763370000000002,"lng":-49.286680000000004}],[{"lat":-16.763370000000002,"lng":-49.286680000000004},{"lat":-16.76329,"lng":-49.28703},{"lat":-16.76323,"lng":-49.28736000000001},{"lat":-16.763150000000003,"lng":-49.287710000000004},{"lat":-16.763070000000003,"lng":-49.288050000000005},{"lat":-16.76291,"lng":-49.28877000000001}],[{"lat":-16.76291,"lng":-49.28877000000001},{"lat":-16.76285,"lng":-49.28876},{"lat":-16.762800000000002,"lng":-49.28876},{"lat":-16.76274,"lng":-49.28877000000001},{"lat":-16.762680000000003,"lng":-49.28878},{"lat":-16.76263,"lng":-49.2888},{"lat":-16.76257,"lng":-49.28882},{"lat":-16.762520000000002,"lng":-49.288850000000004},{"lat":-16.76247,"lng":-49.288880000000006},{"lat":-16.76244,"lng":-49.28891},{"lat":-16.762400000000003,"lng":-49.288940000000004},{"lat":-16.76236,"lng":-49.288970000000006},{"lat":-16.762330000000002,"lng":-49.289},{"lat":-16.7623,"lng":-49.28904000000001},{"lat":-16.76227,"lng":-49.289080000000006},{"lat":-16.76225,"lng":-49.289120000000004},{"lat":-16.762230000000002,"lng":-49.28916},{"lat":-16.76183,"lng":-49.28907},{"lat":-16.76154,"lng":-49.289},{"lat":-16.761180000000003,"lng":-49.288920000000005},{"lat":-16.76089,"lng":-49.28886000000001},{"lat":-16.7606,"lng":-49.2888},{"lat":-16.76022,"lng":-49.28873},{"lat":-16.75986,"lng":-49.28866000000001},{"lat":-16.75888,"lng":-49.288450000000005}],[{"lat":-16.75888,"lng":-49.288450000000005},{"lat":-16.758480000000002,"lng":-49.28839000000001},{"lat":-16.757740000000002,"lng":-49.28821000000001},{"lat":-16.75748,"lng":-49.28815},{"lat":-16.757270000000002,"lng":-49.28810000000001},{"lat":-16.756590000000003,"lng":-49.287940000000006}],[{"lat":-16.756590000000003,"lng":-49.287940000000006},{"lat":-16.756590000000003,"lng":-49.28792000000001},{"lat":-16.756590000000003,"lng":-49.287910000000004},{"lat":-16.756580000000003,"lng":-49.287890000000004},{"lat":-16.756580000000003,"lng":-49.28788},{"lat":-16.75657,"lng":-49.28786},{"lat":-16.75656,"lng":-49.287850000000006},{"lat":-16.75656,"lng":-49.28784},{"lat":-16.75655,"lng":-49.28783000000001},{"lat":-16.75654,"lng":-49.28782},{"lat":-16.75653,"lng":-49.28781000000001},{"lat":-16.756510000000002,"lng":-49.287800000000004},{"lat":-16.756500000000003,"lng":-49.28779},{"lat":-16.756490000000003,"lng":-49.287780000000005},{"lat":-16.75647,"lng":-49.28777},{"lat":-16.75646,"lng":-49.28777},{"lat":-16.75645,"lng":-49.287760000000006},{"lat":-16.75643,"lng":-49.287760000000006},{"lat":-16.756410000000002,"lng":-49.287760000000006},{"lat":-16.756400000000003,"lng":-49.287760000000006},{"lat":-16.75639,"lng":-49.287760000000006},{"lat":-16.75637,"lng":-49.287760000000006},{"lat":-16.75636,"lng":-49.28777},{"lat":-16.75634,"lng":-49.28777},{"lat":-16.756330000000002,"lng":-49.287780000000005},{"lat":-16.756320000000002,"lng":-49.28779},{"lat":-16.756310000000003,"lng":-49.28779},{"lat":-16.75629,"lng":-49.287800000000004},{"lat":-16.75628,"lng":-49.28781000000001},{"lat":-16.75627,"lng":-49.28782},{"lat":-16.75626,"lng":-49.28784},{"lat":-16.75625,"lng":-49.287850000000006},{"lat":-16.75625,"lng":-49.28786},{"lat":-16.756030000000003,"lng":-49.28781000000001},{"lat":-16.75568,"lng":-49.28773},{"lat":-16.75534,"lng":-49.28766},{"lat":-16.75515,"lng":-49.28761},{"lat":-16.75496,"lng":-49.28757},{"lat":-16.75459,"lng":-49.287470000000006},{"lat":-16.75437,"lng":-49.28743},{"lat":-16.7543,"lng":-49.28741},{"lat":-16.75419,"lng":-49.28739},{"lat":-16.75327,"lng":-49.287180000000006},{"lat":-16.753210000000003,"lng":-49.28717},{"lat":-16.752200000000002,"lng":-49.286930000000005},{"lat":-16.75214,"lng":-49.28692},{"lat":-16.751350000000002,"lng":-49.28672},{"lat":-16.75112,"lng":-49.28667},{"lat":-16.750500000000002,"lng":-49.28652},{"lat":-16.75009,"lng":-49.28642000000001},{"lat":-16.749670000000002,"lng":-49.28633000000001},{"lat":-16.74925,"lng":-49.28624000000001}],[{"lat":-16.74925,"lng":-49.28624000000001},{"lat":-16.749180000000003,"lng":-49.28625},{"lat":-16.749100000000002,"lng":-49.286280000000005},{"lat":-16.749010000000002,"lng":-49.286300000000004},{"lat":-16.74884,"lng":-49.286350000000006},{"lat":-16.748240000000003,"lng":-49.286550000000005},{"lat":-16.74789,"lng":-49.28665},{"lat":-16.747780000000002,"lng":-49.286680000000004},{"lat":-16.74772,"lng":-49.28669000000001},{"lat":-16.74755,"lng":-49.286750000000005}],[{"lat":-16.74755,"lng":-49.286750000000005},{"lat":-16.747510000000002,"lng":-49.286680000000004},{"lat":-16.747490000000003,"lng":-49.28665},{"lat":-16.74742,"lng":-49.28651000000001},{"lat":-16.747400000000003,"lng":-49.286460000000005},{"lat":-16.74735,"lng":-49.28638},{"lat":-16.747300000000003,"lng":-49.286280000000005},{"lat":-16.747230000000002,"lng":-49.286170000000006},{"lat":-16.74719,"lng":-49.286080000000005},{"lat":-16.74709,"lng":-49.285900000000005},{"lat":-16.747,"lng":-49.28573},{"lat":-16.74686,"lng":-49.28548000000001},{"lat":-16.74663,"lng":-49.28506},{"lat":-16.74643,"lng":-49.284690000000005},{"lat":-16.74624,"lng":-49.28432},{"lat":-16.74622,"lng":-49.28426},{"lat":-16.7462,"lng":-49.284220000000005},{"lat":-16.746010000000002,"lng":-49.283860000000004},{"lat":-16.745820000000002,"lng":-49.28351000000001},{"lat":-16.745440000000002,"lng":-49.28282},{"lat":-16.74529,"lng":-49.28253},{"lat":-16.74513,"lng":-49.2822},{"lat":-16.74508,"lng":-49.282120000000006},{"lat":-16.744960000000003,"lng":-49.28191},{"lat":-16.74446,"lng":-49.28098000000001},{"lat":-16.74427,"lng":-49.280640000000005},{"lat":-16.744040000000002,"lng":-49.280190000000005},{"lat":-16.744,"lng":-49.28013000000001},{"lat":-16.743840000000002,"lng":-49.27985},{"lat":-16.7436,"lng":-49.279410000000006},{"lat":-16.74339,"lng":-49.27902},{"lat":-16.743380000000002,"lng":-49.27899000000001},{"lat":-16.743360000000003,"lng":-49.278960000000005},{"lat":-16.74324,"lng":-49.27875},{"lat":-16.74311,"lng":-49.278510000000004},{"lat":-16.742990000000002,"lng":-49.278290000000005},{"lat":-16.74294,"lng":-49.278180000000006},{"lat":-16.742880000000003,"lng":-49.27807000000001}],[{"lat":-16.742880000000003,"lng":-49.27807000000001},{"lat":-16.74285,"lng":-49.278020000000005},{"lat":-16.74283,"lng":-49.27798000000001},{"lat":-16.74278,"lng":-49.27801},{"lat":-16.74274,"lng":-49.27803},{"lat":-16.742240000000002,"lng":-49.278290000000005},{"lat":-16.74208,"lng":-49.27837},{"lat":-16.741660000000003,"lng":-49.278580000000005},{"lat":-16.74163,"lng":-49.27859},{"lat":-16.741480000000003,"lng":-49.278670000000005},{"lat":-16.74118,"lng":-49.27881000000001},{"lat":-16.74095,"lng":-49.27892000000001}],[{"lat":-16.74095,"lng":-49.27892000000001},{"lat":-16.740940000000002,"lng":-49.27891},{"lat":-16.740930000000002,"lng":-49.27891},{"lat":-16.740920000000003,"lng":-49.27890000000001},{"lat":-16.740910000000003,"lng":-49.27890000000001},{"lat":-16.74089,"lng":-49.278890000000004},{"lat":-16.74088,"lng":-49.278890000000004},{"lat":-16.74087,"lng":-49.278890000000004},{"lat":-16.740850000000002,"lng":-49.278890000000004},{"lat":-16.74067,"lng":-49.2785},{"lat":-16.740650000000002,"lng":-49.27846},{"lat":-16.740550000000002,"lng":-49.27825000000001},{"lat":-16.7405,"lng":-49.278130000000004},{"lat":-16.74049,"lng":-49.27812},{"lat":-16.740360000000003,"lng":-49.27783},{"lat":-16.74023,"lng":-49.277550000000005},{"lat":-16.740160000000003,"lng":-49.27741},{"lat":-16.7401,"lng":-49.277280000000005}],[{"lat":-16.7401,"lng":-49.277280000000005},{"lat":-16.740540000000003,"lng":-49.27707}]],
                516, '9 mins', 3746, '3.7 km', 14
            )
        }
        const service = require('../services/AdDriverService')
        await service.init()

        return res.send('gotta die sometime')
    } catch (e) {
        return res.jerror(e)
    }
})

// user register
router.post('/user/register', ApiRegisterController.register)
router.post('/user/register/check', ApiRegisterController.check)

router.post('/geo/distance', ApiGeoController.distance)
router.post('/geo/route', ApiGeoController.route)
router.post('/geo/full', ApiGeoController.full)

// mobile
router.post('/user/mobile', authMid(), ApiUserController.mobileService)

// user
router.get('/myself', authMid(), ApiUserController.myself)
router.get('/status', authMid(), ApiUserController.status)
router.post('/user/update-name-avatar', authMid(), ApiUserController.updateNameAvatar)

// driver
router.get('/driver/myself', authMid(), ApiDriverController.myself)
router.post('/driver/request', authMid(), ApiDriverController.request)
router.post('/driver/request/validate', authMid(true), ApiDriverController.requestValidate)

//
router.post('/driver/position', authMid(), ApiDriverController.position)
router.post('/driver/online', authMid(), ApiDriverController.online)

// driver car
router.get('/driver/car', authMid(), ApiDriverCarController.index)
router.post('/driver/car/store', authMid(), ApiDriverCarController.store)
router.delete('/driver/car/:carId', authMid(), ApiDriverCarController.destroy)

// comments
router.get('/comment', authMid(), ApiCommentController.index)
router.post('/comment', authMid(), ApiCommentController.store)
router.delete('/comment/:commentId', authMid(true), ApiCommentController.destroy)

// social
router.get('/social', authMid(), ApiSocialController.index)

// ad
router.get('/ad', authMid(), ApiAdController.index)
router.get('/ad/driver', authMid(), ApiAdController.indexDriver)
router.get('/ad/:id', authMid(), ApiAdController.find)
router.post('/ad', authMid(), ApiAdController.store)
router.delete('/ad', authMid(), ApiAdController.close)

// ad proposal
router.get('/ad/:ad_id/proposal', authMid(), ApiAdProposalController.index)
router.post('/ad/:ad_id/proposal', authMid(), ApiAdProposalController.store)
router.put('/ad/:ad_id/proposal/:proposal_id', authMid(), ApiAdProposalController.update)

// hashes
router.get('/hash/:key', ApiHashController.index)

// ad hire
router.get('/contract', authMid(), ApiContractController.index)
router.post('/contract', authMid(), ApiContractController.store)
router.put('/contract/:contract_id/start', authMid(), ApiContractController.start)
router.put('/contract/:contract_id/finish', authMid(), ApiContractController.finish)