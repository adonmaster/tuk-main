import {Model} from "sequelize";

export default class AdProposal extends Model {

    id: number

    ad_id: number
    ad_user_id: number

    driver_id: number
    driver_user_id: number
    driver_name_first: string
    driver_name_last: string
    driver_pos_lat: number
    driver_pos_lng: number
    driver_avatar_thumb: string
    driver_car: string

    price: number
    arrival: number

    created_at: string
    updated_at: string

}