import {Model} from "sequelize";

export default class Interest extends Model {

    id: number

    subject_name: string
    subject_id: number

    created_at: string
    updated_at: string

}