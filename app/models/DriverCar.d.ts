import {Model} from "sequelize";
import Driver from "./Driver";

export default class DriverCar extends Model {

    id: number
    driver_id: number
    driver: Driver

    plate: string
    brand: string
    model: string
    color: string
    img: string
    img_uid: string

    created_at: Date
}