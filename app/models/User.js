'use strict';
const { Model } = require('sequelize');
const UserPresenter = require('./UserPresenter')

module.exports = (sequelize, DataTypes) => {

    class User extends Model {
        static associate(models) {
        }
    }

    User.init({

        is_admin: {type: DataTypes.BOOLEAN, defaultValue: false},

        email: DataTypes.STRING,
        name: DataTypes.STRING,
        password: DataTypes.STRING,
        token: DataTypes.STRING,

        mobile_token: DataTypes.STRING,
        mobile_service: DataTypes.STRING,

        status: DataTypes.STRING,

        avatar_thumb: DataTypes.STRING,
        avatar_full: DataTypes.STRING,
        avatar_uid: DataTypes.STRING,

        p: {
            type: DataTypes.VIRTUAL,
            get() { return new UserPresenter(this); }
        },

    }, {
        sequelize,
        modelName: 'User', tableName: 'users',
        createdAt: 'created_at', updatedAt: 'updated_at',
        defaultScope: {
            // attributes: {exclude: ['password', 'token', 'is_admin', 'mobile_service', 'mobile_token']}
        },
        scopes: {
            safe: {
                attributes: {exclude: ['password', 'token', 'is_admin', 'mobile_service', 'mobile_token']}
            }
        }
    })

    return User;
};