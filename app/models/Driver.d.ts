import {Model} from "sequelize";
import User from "./User";
import Social from "./Social";

export default class Driver extends Model {

    id: number

    user: User
    social: Social

    user_id: number

    name_first: string
    name_last: string
    cpf: string

    ref_city: string
    ref_uf: string

    avatar: string
    avatar_thumb: string
    avatar_uid: string

    pos_lat: number
    pos_lng: number

    car_id: number

    active: boolean
    online: boolean
    points: number

    updated_at: string
    created_at: string

}