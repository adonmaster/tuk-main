'use strict';
const { Model, Op } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Ad extends Model {
    static associate(models) {
      this.belongsTo(models.User, {
        as: 'user', foreignKey: 'user_id'
      })
    }
  }

  Ad.init({

    user_id: DataTypes.INTEGER,
    user_name: DataTypes.STRING,

    origin_lat: DataTypes.DOUBLE,
    origin_lng: DataTypes.DOUBLE,
    origin_s: DataTypes.STRING,

    destination_lat: DataTypes.DOUBLE,
    destination_lng: DataTypes.DOUBLE,
    destination_s: DataTypes.STRING,

    route: {
      type: DataTypes.TEXT,
      get() {
        const v = this.getDataValue('route')
        return v ? JSON.parse(v) : null
      },
      set(v) {
        this.setDataValue('route', JSON.stringify(v))
      }
    },

    duration: DataTypes.INTEGER,
    duration_s: DataTypes.STRING,

    distance: DataTypes.INTEGER,
    distance_s: DataTypes.STRING,

    closed_at: DataTypes.DATE,

    price: DataTypes.DECIMAL
  }, {
    sequelize, tableName: 'ads', modelName: 'Ad', updatedAt: false, createdAt: 'created_at',
    scopes: {
      active: {
        where: {closed_at: {[Op.is]: null}}
      }
    }
  });

  return Ad;
};