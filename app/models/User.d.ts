import { Model } from "sequelize";
import UserPresenter from './UserPresenter'

export default class User extends Model {

    id: bigint

    is_admin: boolean
    email: string
    name: string

    password: string
    token: string

    mobile_token: string
    mobile_service: string

    status: string

    avatar_thumb: string
    avatar_full: string
    avatar_uid: string

    created_at: string
    updated_at: string

    p: UserPresenter
}