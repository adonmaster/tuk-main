'use strict';
const { Model, Op } = require('sequelize');



module.exports = (sequelize, DataTypes) => {

  class Contract extends Model {
    static associate(models) {}
  }

  Contract.init({

    ad_id: DataTypes.INTEGER,
    ad_user_id: DataTypes.INTEGER,
    driver_id: DataTypes.INTEGER,
    driver_user_id: DataTypes.INTEGER,
    price: DataTypes.DOUBLE,

    run_ini: DataTypes.DATE,
    run_end: DataTypes.DATE

  }, {
    sequelize, modelName: 'Contract', tableName: 'contracts', createdAt: 'created_at', updatedAt: 'updated_at',

    scopes: {
      pending: {
        where: {
          [Op.or]: [
            {run_ini: {[Op.is]: null}},
            {run_end: {[Op.is]: null}}
          ]
        }
      },
      running: {
        where: {
          run_ini: {[Op.not]: null},
          run_end: {[Op.is]: null}
        }
      }
    }

  });

  return Contract;
};