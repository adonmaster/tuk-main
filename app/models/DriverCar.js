'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  class DriverCar extends Model {
    static associate(models) {}
  }

  DriverCar.init({

    driver_id: DataTypes.INTEGER,

    plate: DataTypes.STRING,
    brand: DataTypes.STRING,
    model: DataTypes.STRING,
    color: DataTypes.STRING,
    img: DataTypes.STRING,
    img_uid: DataTypes.STRING

  }, {
    sequelize, modelName: 'DriverCar', tableName: 'driver_car', createdAt: 'created_at', updatedAt: false
  });

  return DriverCar;
};