import {Model} from "sequelize";

export default class Contract extends Model {

    id: number
    ad_id: number
    ad_user_id: number
    driver_id: number
    driver_user_id: number
    price: number

    run_ini: string
    run_end: string

    created_at: string
    updated_at: string

}