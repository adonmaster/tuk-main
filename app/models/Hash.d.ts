import {Model} from "sequelize";

export default class Hash extends Model {

    id: number
    key: string
    hash: string
    param_i: number

}