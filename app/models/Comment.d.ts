import {Model} from "sequelize";
import User from "./User";

export default class Comment extends Model {

    from: User
    to: User

    from_user_id: number
    to_user_id: number
    body: string
    is_positive: boolean
    created_at: string

}