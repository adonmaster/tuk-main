import { Model } from "sequelize";
import User from "./User";

export default class Social extends Model{

    user: User
    user_id: number

    like: number
    dislike: number
    comment_positive: number
    comment_negative: number

}