'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  class AdProposal extends Model {
    static associate(models) { }
  }

  AdProposal.init({

    ad_id: { type: DataTypes.INTEGER },
    ad_user_id: { type: DataTypes.INTEGER },

    driver_id: { type: DataTypes.INTEGER },
    driver_user_id: { type: DataTypes.INTEGER },
    driver_name_first: { type: DataTypes.STRING },
    driver_name_last: { type: DataTypes.STRING },
    driver_pos_lat: { type: DataTypes.DOUBLE },
    driver_pos_lng: { type: DataTypes.DOUBLE },
    driver_avatar_thumb: { type: DataTypes.STRING },
    driver_car: { type: DataTypes.STRING },

    price: { type: DataTypes.DECIMAL },
    arrival: { type: DataTypes.INTEGER },

  }, {
    sequelize,
    modelName: 'AdProposal', tableName: 'ad_proposal', createdAt: 'created_at', updatedAt: 'updated_at'
  });

  return AdProposal;
};