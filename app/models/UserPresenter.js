const Str = require('../helpers/Str')
const _omit = require('lodash/omit')

const UserPresenter = function(model) {

    this.emailObfuscated = () => Str.obfuscateEmail(model.email)

    this.toJsonSafe = () => {
        return _omit(model.toJSON(), [
            'password', 'token', 'is_admin', 'mobile_service', 'mobile_token'
        ])
    }

}

module.exports = UserPresenter