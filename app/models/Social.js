'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  class Social extends Model {
    static associate(models) {
      this.belongsTo(models.User, {as: 'user', foreignKey: 'user_id'})
    }
  }

  Social.init({

    user_id: DataTypes.INTEGER,
    like: DataTypes.INTEGER,
    dislike: DataTypes.INTEGER,
    comment_positive: DataTypes.INTEGER,
    comment_negative: DataTypes.INTEGER

  }, {
    sequelize, modelName: 'Social', tableName: 'social', timestamps: false
  });

  return Social;
};