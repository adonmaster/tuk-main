'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class InterestItem extends Model {
    static associate(models) {
    }
  }
  InterestItem.init({
    subject_name: DataTypes.STRING,
    subject_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    pinned: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'InterestItem', tableName: 'interest_item', createdAt: 'created_at', updatedAt: 'updated_at'
  });
  return InterestItem;
};