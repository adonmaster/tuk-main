'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    static associate(models) {
      this.belongsTo(models.User, {as: 'from', foreignKey: 'from_user_id'})
      this.belongsTo(models.User, {as: 'to', foreignKey: 'to_user_id'})
    }
  }
  Comment.init({

    from_user_id: DataTypes.INTEGER,
    to_user_id: DataTypes.INTEGER,
    body: DataTypes.STRING,
    is_positive: {type: DataTypes.BOOLEAN, defaultValue: true}

  }, { sequelize, modelName: 'Comment', tableName: 'comments', createdAt: 'created_at', updatedAt: false});
  return Comment;
};