import {Model} from "sequelize";
import User from "./User";

export default class DriverRequest extends Model {

    id: number
    user: User
    user_id: number

    avatar: string
    avatar_thumb: string
    avatar_uid: string

    name_first: string
    name_last: string

    gender: string
    birth: string
    cpf: string

    ref_city: string
    ref_uf: string

    license_no: string
    license_valid_until: string
    license_img: string
    license_img_uid: string

    legal_type: string
    legal_img: string
    legal_img_uid: string

    car_brand: string
    car_model: string
    car_color: string
    car_plate: string
    car_img: string
    car_img_uid: string
    car_doc_img: string
    car_doc_img_uid: string

    created_at: string
    updated_at: string

}