'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

    class SubjectCode extends Model {
        static associate(models) {
        }
    }

    SubjectCode.init({
        subject: DataTypes.STRING,
        code: DataTypes.STRING,
        expired_at: DataTypes.DATE,
    }, {
        sequelize,
        timestamps: false,
        modelName: 'SubjectCode', tableName: 'subject_code',
    })

    return SubjectCode
};