'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Hash extends Model {
    static associate(models) {}
  }

  Hash.init({

    key: DataTypes.STRING,
    hash: DataTypes.STRING,
    param_i: DataTypes.INTEGER

  }, { sequelize, modelName: 'Hash', tableName: 'hashes', timestamps: false });

  return Hash;
};