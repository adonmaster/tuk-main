'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Interests extends Model {
    static associate(models) {
    }
  }

  Interests.init({
    subject_name: DataTypes.STRING,
    subject_id: DataTypes.INTEGER
  }, { sequelize, modelName: 'Interest',  tableName: 'interests', updatedAt: 'updated_at', createdAt: 'created_at' });

  return Interests;
};