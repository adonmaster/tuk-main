'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Driver extends Model {
    static associate(models) {
      this.belongsTo(models.User, {as: 'user', foreignKey: 'user_id'})
      this.hasMany(models.DriverCar, {as: 'cars', foreignKey: 'driver_id'})
      this.belongsTo(models.DriverCar, {as: 'car', foreignKey: 'car_id'})
      this.hasOne(models.Social, {as: 'social', sourceKey: 'user_id', foreignKey: 'user_id'})
    }
  }
  Driver.init({

    user_id: DataTypes.INTEGER,

    name_first: DataTypes.STRING,
    name_last: DataTypes.STRING,
    cpf: DataTypes.STRING,

    ref_city: DataTypes.STRING,
    ref_uf: DataTypes.STRING,

    avatar: DataTypes.STRING,
    avatar_thumb: DataTypes.STRING,
    avatar_uid: DataTypes.STRING,

    pos_lat: { type: DataTypes.DOUBLE, defaultValue: 0 },
    pos_lng: { type: DataTypes.DOUBLE, defaultValue: 0 },

    car_id: DataTypes.INTEGER,

    active: DataTypes.BOOLEAN,
    online: {type: DataTypes.BOOLEAN, defaultValue: false },
    points: { type: DataTypes.INTEGER, defaultValue: 0 },

  }, {
    sequelize, modelName: 'Driver', tableName: 'drivers', createdAt: 'created_at', updatedAt: 'updated_at',
    scopes: {
      active: {
        where: {active: true}
      },
      online: {
        where: {active: true, online: true}
      }
    }
  });
  return Driver;
};