'use strict';
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {

    class DriverRequest extends Model {
        static associate(models) {
            this.belongsTo(models.User, {as: 'user', foreignKey: 'user_id'})
        }
    }

    DriverRequest.init({

        user_id: DataTypes.INTEGER,

        avatar: DataTypes.STRING,
        avatar_thumb: DataTypes.STRING,
        avatar_uid: DataTypes.STRING,

        name_first: DataTypes.STRING,
        name_last: DataTypes.STRING,

        gender: DataTypes.STRING,
        birth: DataTypes.DATE,
        cpf: DataTypes.STRING,

        ref_city: DataTypes.STRING,
        ref_uf: DataTypes.STRING,

        license_no: DataTypes.STRING,
        license_valid_until: DataTypes.DATE,
        license_img: DataTypes.STRING,
        license_img_uid: DataTypes.STRING,

        legal_type: DataTypes.STRING,
        legal_img: DataTypes.STRING,
        legal_img_uid: DataTypes.STRING,

        car_brand: DataTypes.STRING,
        car_model: DataTypes.STRING,
        car_color: DataTypes.STRING,
        car_plate: DataTypes.STRING,
        car_img: DataTypes.STRING,
        car_img_uid: DataTypes.STRING,
        car_doc_img: DataTypes.STRING,
        car_doc_img_uid: DataTypes.STRING,

        closed_at: {type: DataTypes.DATE, defaultValue: null},

    }, {
        sequelize,
        modelName: 'DriverRequest', tableName: 'driver_request', createdAt: 'created_at', updatedAt: 'updated_at'
    });
    return DriverRequest;
};