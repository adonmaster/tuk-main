import { Model } from 'sequelize'

export default class Ad extends Model {

    id: number
    user_id: number
    user_name: string

    origin_lat: number
    origin_lng: number
    origin_s: string

    destination_lat: number
    destination_lng: number
    destination_s: string

    route: string

    duration: number
    duration_s: string

    distance: number
    distance_s: string

    closed_at?: string

    price: number

    created_at: string
}