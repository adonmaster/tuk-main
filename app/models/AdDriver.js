'use strict';
const { Model } = require('sequelize');


module.exports = (sequelize, DataTypes) => {

  class AdDriver extends Model {
    static associate(models) {}
  }

  AdDriver.init({

    ad_id: DataTypes.INTEGER,
    driver_id: DataTypes.INTEGER

  }, { sequelize, modelName: 'AdDriver', tableName: 'ad_driver', timestamps: false});

  return AdDriver;
};