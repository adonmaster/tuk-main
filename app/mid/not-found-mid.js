const NotFoundMid = async (req, res) => {
    return res.jerr(`Not found, brother! ${req.originalUrl}`, 404)
}

module.exports = NotFoundMid