
// utils
function extractBearerToken(req) {
    const bearer = req.header('Authorization') || ''
    const bearerComponents = bearer.split(' ')
    return bearerComponents.length > 1 ? bearerComponents[1] : null
}

const SessionService = require('../services/SessionService')

// class
module.exports = (isAdmin=false) => {
    return async function(req, res, next) {

        const token = extractBearerToken(req)
        let msg = 'Sem autorização'

        // token - mobile
        if (token) {
            const model = await SessionService.userFrom(token)
            if (model) {
                if (!isAdmin || (isAdmin && model.is_admin)) {
                    req.auth_id = model.id
                    req.auth_user = model
                    return next()
                } else {
                    msg = 'Você precisa ser admin'
                }
            }
        }

        return res.jerr(msg, 401)
    }
}