class ModelNotFoundError extends Error {

    static t(msg='Modelo não encontrado') {
        throw new ModelNotFoundError(msg)
    }

    constructor(message) {
        super(message)
        this.name = this.constructor.name
    }

}

module.exports = ModelNotFoundError