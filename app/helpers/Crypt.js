const crypto = require('crypto')

module.exports = {

    async hash(password) {
        return new Promise((resolve, reject) => {
            const salt = process.env.APP_SECRET_KEY
            crypto.scrypt(password, salt, 10, (err, derivedKey) => {
                if (err) reject(err);
                resolve(derivedKey.toString('hex'))
            });
        })
    },

    async verify(password, hashedPassword) {
        return hashedPassword === await this.hash(password)
    },

    random(size) {
        return crypto.randomBytes(size/2).toString('hex')
    }

}