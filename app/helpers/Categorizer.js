const Categorizer = {

    /**
     *
     * @param {[]} list
     * @param {Function} cb
     * @returns CategorizerResult
     */
    process(list, cb) {
        const r = []
        for (const item of list) {
            const category = cb(item)
            const index = r.findIndex(({category:c}) => category === c)
            if (~index) {
                r[0].items.push(item)
            } else {
                r.push({category, items: [item]})
            }
        }
        return new CategorizerResult(r)
    }

}

class CategorizerResult {

    constructor(r) { this.r = r }

    items(category) {
        const index = this.r.findIndex(({category:c}) => c===category)
        if (~index) return this.r[index].items
        return []
    }
}

module.exports = Categorizer