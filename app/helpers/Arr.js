class Arr {

    /**
     *
     * @param {array} items
     */
    constructor(items) {
        this.items = items
    }

    last(def=null) {
        const len = this.items.length
        if (len === 0) return def
        return this.items[len - 1]
    }

}


/**
 *
 * @param {[]} items
 * @returns {Arr}
 */
function arr(items) {
    return new Arr(items)
}

module.exports = {Arr, arr}