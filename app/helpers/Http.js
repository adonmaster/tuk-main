const _ = require('lodash')

const Http = {

    translateAxiosError(e) {
        const obj = _.get(e, ['response', 'data', 'payload', 'errors'], null)
        if ( ! obj) return e.message

        const fields = Object.keys(obj)
        if (fields.length === 0) return e.message

        return `${e.response.status}: ${obj[fields[0]][0]}`
    }

}

module.exports = Http