if ( ! process.env.GEO_DISTANCE_FACTOR) throw Error('.env falta GEO_DISTANCE_FACTOR')

const FACTOR = parseFloat(process.env.GEO_DISTANCE_FACTOR)
const FACTOR_7 = FACTOR * .7
const FACTOR_3 = FACTOR * .3

const Geo = { FACTOR, FACTOR_3, FACTOR_7 }

module.exports = Geo