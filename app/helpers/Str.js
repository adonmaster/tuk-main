const Str = {

    digitsOnly(s) {
        let numb = String(s).match(/\d/g)
        if (numb) return numb.join("")
        return ''
    },

    /**
     *
     * @param {string|number} s
     * @param {number} len
     * @param {string} filling
     * @return {string}
     */
    padLeftFixed(s, len, filling=' ') {
        const base = filling.repeat(len)
        return (base + s).slice(-len)
    },

    obfuscateEmail(s)
    {
        if ( ! s) return '[*]'
        const components = s.split('@')
        if (components.length <= 1) return '[*]'

        const user = components[0]
        const domain = components[1]
        const ouser = user.substr(0, 3) + '*'.repeat(user.length - 3)

        return `${ouser}@${domain}`
    },

    ellipsizeStart(s, len) {
        return s.slice(0, len)
    }
}

module.exports = Str