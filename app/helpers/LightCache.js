class LightCache {

    /**
     *
     * @param {int} capacity
     */
    constructor(capacity=200) {
        this.capacity = capacity
        this.cache = []
    }

    /**
     *
     * @param key
     * @param {function:Promise<*>} cbPayload
     * @returns {Promise<*>}
     */
    async retrieve(key, cbPayload)
    {
        const item = this.cache.find(({key:k}) => k===key)
        if (item) return item.payload

        const payload = await cbPayload()
        this.cache.push({key, payload})

        if (this.cache.length > this.capacity) this.cache.shift()

        return payload
    }

    get(key, def=null) {
        const item = this.cache.find(({key:k}) => k===key)
        if (item) return item.payload
        return def
    }

    /**
     *
     * @param key
     * @param {function:Promise<*>} cbPayload
     * @param {boolean} forceRefresh
     */
    async validate(key, cbPayload, forceRefresh)
    {
        const item = this.cache.find(({key:k}) => k===key)
        if (item && !forceRefresh) return

        const payload = await cbPayload()
        this.cache.push({key, payload})
    }

    /**
     *
     * @param {*|null} key
     */
    invalidate(key=null) {
        if ( ! key) {
            this.cache = []
            return
        }
        this.cache = this.cache.filter(({key:k}) => key!==k)
    }

    filter(cb) {
        this.cache = this.cache.filter(({key, payload}) => cb(key, payload))
    }
}

module.exports = LightCache