const winston = require('winston')
const path = require('path')

module.exports = {

    init() {

        const dir = path.resolve(__dirname, '../../storage/logs/')

        const logger = winston.createLogger({
            format: winston.format.combine(
                winston.format.errors({stack: true}),
                winston.format.json()
            ),
            transports: [
                new winston.transports.File({ dirname: dir, filename: 'error.log', level: 'error' }),
                new winston.transports.File({ dirname: dir, filename: 'combined.log' }),
            ]
        })

        if (process.env.NODE_ENV !== 'production') {
            logger.add(new winston.transports.Console({
                format: winston.format.simple()
            }))
        }

        this.logger = logger
    },

    /**
     *
     * @param {string} message
     * @param {Object} obj
     */
    e(message, obj={}) {
        this.logger.error(message, obj)
    }

}