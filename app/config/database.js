const fs = require('fs');
const path = require('path');

module.exports = {

    use_env_variable: false,

    development: {
        dialect: 'sqlite',
        storage: path.resolve('storage', 'db-dev.sqlite'),
        define: { underscored: true }
    },
    
    test: {
        username: process.env.CI_DB_USERNAME,
        password: process.env.CI_DB_PASSWORD,
        database: process.env.CI_DB_NAME,
        host: '127.0.0.1',
        port: 3306,
        dialect: 'mysql',
        dialectOptions: {
            bigNumberStrings: true
        }
    },
    
    production: {
        dialect: 'mysql',
        host: process.env.MYSQL_HOST,
        database: process.env.MYSQL_DB,
        username: process.env.MYSQL_USER,
        password: process.env.MYSQL_PWD,
        port: 3306
    }
};