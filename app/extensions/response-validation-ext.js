const Validator = require('validatorjs')
const { Sanitizer } = require('../helpers/Sanitizer')
const ModelNotFoundError = require('../errors/ModelNotFoundError')
const _merge = require('lodash/merge')

module.exports = app => {

    app.response.j = function (message, status=200, payload={}) {
        return this.status(status).json({message, payload})
    }

    app.response.jok = function(message='Ok', status=200, payload={}) {
        return this.j(message, status, payload)
    }

    app.response.jerr = function(message, status=400, payload={}) {
        return this.j(message, status, payload)
    }

    /**
     *
     * @param {Error} error
     * @param {*} payload
     */
    app.response.jerror = function(error, payload={}) {
        let status = 400
        if (error instanceof ModelNotFoundError) status = 404
        return this.jerr(error.message, status, payload)
    }

    app.response.jpay = function(payload, message='Ok!', status=200) {
        return this.j(message, status, payload)
    }

    app.response.validate = function (rules={}, moreBody={})
    {
        const body = _merge({}, this.req.params, this.req.body, moreBody)
        const validator = new Validator(body, rules)

        if (validator.fails()) {
            this.j('Erro de validação', 422, validator.errors)
            return false
        }

        return body
    }

    app.response.validateAndSanitize = function(rules, sanitizeRules)
    {
        const body = this.validate(rules)
        if (body === false) return false

        return Sanitizer.run(body, sanitizeRules)
    }
}
