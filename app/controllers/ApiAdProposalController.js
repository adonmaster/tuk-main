const Repo = require('../repos/Repo')
const MobileService = require("../services/MobileService");
const { Sanitizer } = require('../helpers/Sanitizer')


let self
const ApiAdProposalController = self = {

    async index(req, res)
    {
        const data = res.validate({
            'ad_id': 'required'
        })
        if (data === false) return

        const { ad_id } = data
        const pinned = req.query['pinned'] == 'true'

        try {

            const r = await Repo.adProposal.getFromAd(ad_id)

            // interests!
            await Repo.interest.saveItem('ad_proposal', ad_id, req.auth_id, pinned)

            return res.json(r) // response

        } catch (e) {
            return res.status(400).send(e.message)
        }
    },

    async store(req, res)
    {
        const data = res.validateAndSanitize({
            'ad_id': 'required',
            'price': 'required',
            'arrival': 'required'
        }, {
            'ad_id': Sanitizer.rules.int
        })
        if (data === false) return

        const { ad_id, price, arrival } = data

        try {

            //
            const ad = await Repo.ad.findOrFail(ad_id, ['id', 'user_id'])
            const driver = await Repo.driver.findByUserOrFail(req.auth_id)
            const car = await Repo.driverCar.findOrFail(driver.car_id)
            const carStr = `${car.plate}, ${car.brand} - ${car.model}`

            const proposal = await Repo.adProposal.save(
                ad_id, ad.user_id, driver.id, req.auth_id, driver.name_first, driver.name_last, driver.pos_lat, driver.pos_lng,
                driver.avatar_thumb, carStr, price, arrival
            )

            //
            await Repo.interest.saveItem('ad_proposal', ad_id, req.auth_id, true)
            await self.__updateInterestAndNotifyUsers(ad_id)

            return res.jpay(proposal)

        } catch (e) {
            return res.jerror(e)
        }

    },

    async update(req, res)
    {
        const data = res.validateAndSanitize({
            'ad_id': 'required',
            'proposal_id': 'required',
            'price': 'required',
            'arrival': 'required'
        }, {
            ad_id: Sanitizer.rules.int
        })
        if (data === false) return

        const {ad_id, proposal_id, price, arrival} = data

        try {

            //
            const affected = await Repo.adProposal.update(proposal_id, price, arrival)

            // interests
            await Repo.interest.saveItem('ad_proposal', ad_id, req.auth_id, true)
            await self.__updateInterestAndNotifyUsers(ad_id)

            return res.jpay({affected: affected[0]})
        } catch (e) {
            return res.jerror(e)
        }
    },

    async __updateInterestAndNotifyUsers(ad_id) {
        const user_ids = await Repo.interest.saveHeaderAndFindInterestUserIdList('ad_proposal', ad_id)
        await MobileService.send(user_ids, null, null, `ad_proposal:${ad_id}`)
    }
}

module.exports = ApiAdProposalController