const Repo = require('../repos/Repo')
const MobileService = require("../services/MobileService");

const ApiCommentController = {

    async index(req, res) {
        try {

            return res.json(await Repo.comment.getAllWithSafeUser(req.auth_id))

        } catch(e) {
            return res.jerror(e)
        }
    },

    async store(req, res)
    {
        const data = res.validate({
            to_user_id: 'required',
            body: 'required',
            is_positive: 'required'
        })
        if (data === false) return

        const { to_user_id, body, is_positive } = data

        try {

            // save comment
            const comment = await Repo.comment.create(req.auth_id, to_user_id, body, is_positive)

            const user = await Repo.user.findGraphOrFail(to_user_id, ['name'])

            const mobileBody = `${user.name} comentou sobre você!`
            await MobileService.send([to_user_id], 'Comentário', mobileBody, 'comment')

            //
            return res.jpay(comment)

        } catch(e) {
            return res.jerror(e)
        }
    },

    async destroy(req, res) {
        try {
            await Repo.comment.destroy(req.params['commentId'])
            return res.jok()
        } catch (e) {
            return res.jerror(e)
        }
    }
}

module.exports = ApiCommentController