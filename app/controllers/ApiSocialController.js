const Repo = require('../repos/Repo')

const ApiSocialController = {

    async index(req, res) {
        try {
            const repoResponse = await Repo.social.findOrCreate(req.auth_id)
            return res.send(repoResponse[0])
        } catch (e) {
            return res.jerror(e)
        }
    }

}

module.exports = ApiSocialController