const Repo = require('../repos/Repo')
const AdDriverService = require("../services/AdDriverService");
const SessionService = require("../services/SessionService");

const ApiAdController = {

    async index(req, res)
    {
        try {

            const data = await Repo.ad.findActive(req.auth_id)

            return res.send(data)

        } catch (e) {
            return res.jerror(e)
        }
    },

    async indexDriver(req, res)
    {
        try {

            const driverId = await Repo.driver.userIdToDriverIdOrFail(req.auth_id)
            const adDrivers = await Repo.adDriver.getFromDriver_Ads(driverId, true)
            return res.json(adDrivers)

        } catch (e) {
            return res.status(400).send(e.message)
        }
    },

    async find(req, res)
    {
        const data = res.validate({
            id: 'required'
        })
        if (data === false) return

        const { id } = data

        try {

            const json = await Repo.ad.findOrFail(id)
            return res.json(json)

        } catch (e) {
            return res.status(400).send(e.message)
        }
    },


    async store(req, res)
    {
        const data = res.validate({

            'origin_lat': 'required|numeric',
            'origin_lng': 'required|numeric',
            'origin_s': 'required',

            'destination_lat': 'required|numeric',
            'destination_lng': 'required|numeric',
            'destination_s': 'required',

            'route.*.*.lat': 'required|numeric',
            'route.*.*.lng': 'required|numeric',

            'duration': 'required|numeric',
            'duration_s': 'required',

            'distance': 'required|numeric',
            'distance_s': 'required',

            'price': 'required|numeric'
        })
        if (data === false) return

        const {
            origin_lat, origin_lng, origin_s, destination_lat, destination_lng, destination_s,
            route, duration, duration_s, distance, distance_s, price
        } = data

        try {

            //
            let ad = await Repo.ad.findActive(req.auth_id, ['id'])
            if (ad) return res.jerr('Você já tem anúncio ativo.')

            //
            ad = await Repo.ad.create(
                req.auth_id, req.auth_user.name, origin_lat, origin_lng, origin_s,
                destination_lat, destination_lng, destination_s,
                route, duration, duration_s, distance, distance_s, price
            )

            //
            AdDriverService.notify(ad.toJSON())

            //
            await Repo.user.statusAd(req.auth_id)
            SessionService.invalidateById(req.auth_id)

            return res.jpay(ad)

        } catch (e) {
            return res.jerror(e)
        }
    },

    async close(req, res)
    {
        const data = res.validate({
            'ad_id': 'required'
        })
        if (data === false) return

        const { ad_id } = data

        try {

            if ( ! await Repo.ad.existsFromUserId(ad_id, req.auth_id)) {
                return res.jerr('Anúncio não encontrado', 404)
            }

            await AdDriverService.invalidate([ad_id])

            return res.jok()

        } catch (e) {
            return res.jerror(e)
        }
    }
}

module.exports = ApiAdController