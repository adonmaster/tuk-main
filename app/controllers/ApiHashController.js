const Repo = require('../repos/Repo')

const ApiHashController = {

    async index(req, res) {
        const key = req.params['key'] || ''
        const model = await Repo.hash.find(key)
        const r = model ? model.hash : ''
        return res.send(r)
    }

}

module.exports = ApiHashController