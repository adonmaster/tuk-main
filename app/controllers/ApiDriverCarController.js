const Repo = require("../repos/Repo");
const MediaService = require("../services/MediaService");

const ApiDriverCarController = {

    async index(req, res)
    {
        try {

            const driver = await Repo.driver.findByUserOrFail(req.auth_id, ['id', 'user_id'])
            return res.json(await Repo.driverCar.getFrom(driver.id))

        } catch(e) {
            return res.jerror(e)
        }
    },

    async store(req, res)
    {
        const data = res.validate({
            'plate': 'required',
            'brand': 'required',
            'model': 'required',
            'color': 'required',
            'img': 'required',
            'img_uid': 'required'
        })
        if (data === false) return

        const { plate, brand, model, color, img, img_uid } = data

        try {

            const driverId = await Repo.driver.userIdToDriverIdOrFail(req.auth_id)

            const car = await Repo.driverCar.save(driverId, plate, brand, model, color, img, img_uid)

            //
            await MediaService.stashUidFlush()

            return res.jpay(car)

        } catch(e) {
            return res.jerror(e)
        }
    },

    async destroy(req, res)
    {
        const carId = req.params['carId']

        try {

            const car = await Repo.driverCar.find(carId)
            if ( ! car) return res.jerr('Car model not found', 404)

            //
            const driverId = await Repo.driver.userIdToDriverIdOrFail(req.auth_id)
            if (car.driver_id !== driverId) return res.jerr('Esse carro não pertence a este motorista.')

            //
            const cars = await Repo.driverCar.getFrom(driverId)
            const carsFiltered = cars.filter(c => c.id != carId)
            if (carsFiltered.length == 0) return res.jerr('Você não pode remover o único veículo que possui. Adicione um primeiro')

            // destroying && updating def car to driver
            await Repo.driverCar.destroy(carId)
            await Repo.driver.updateDefCar(driverId, carsFiltered[0].id)

            return res.jok()

        } catch (e) {
            return res.jerror(e)
        }
    }

}

module.exports = ApiDriverCarController