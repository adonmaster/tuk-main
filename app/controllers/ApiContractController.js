const Repo = require('../repos/Repo')
const MobileService = require("../services/MobileService");
const SessionService = require("../services/SessionService");

const ApiContractController = {

    async index(req, res) {
        const model = await Repo.contract.findPending(req.auth_id)
        return res.send(model)
    },

    async store(req, res)
    {
        try {

            const ad_user_id = req.auth_id
            const ad_user = req.auth_user

            let contract = await Repo.contract.findPending(ad_user_id)
            if (contract) return res.jerr(`Você já tem uma corrida ativa. [ad:${contract.ad_id}]`)

            //
            const data = res.validate({
                'ad_id': 'required',
                'driver_id': 'required',
                'price': 'required'
            })
            if (data === false) return

            //
            const {ad_id, driver_id, price} = data

            //
            const ad = await Repo.ad.findOrFail(ad_id)
            if (ad.user_id != ad_user_id) return res.jerr('Este não parece ser seu anúncio!')

            //
            const driver = await Repo.driver.findByUserOrFail(driver_id)
            if ( ! driver.active) return res.jerr('O motorista não parece estar ativo!')
            if ( ! driver.online) return res.jerr('O motorista não parece estar online.')

            //
            contract = await Repo.contract.findRunningForDriver(driver_id)
            if (contract) return res.jerr('O motorista já está em uma corrida.')

            //
            contract = await Repo.contract.store(ad_id, ad_user_id, driver_id, driver.user_id, price)

            // status
            await Repo.user.statusContractDriver(ad_user_id)
            SessionService.invalidateById(ad_user_id)

            // notify
            const msg = `${ad_user.name} quer te contratar!`
            MobileService.send([driver.user_id], 'Corrida!', msg, 'contract_driver').catch()

            //
            return res.jpay(contract)

        } catch (e) {
            return res.jerror(e)
        }

    },

    async start(req, res)
    {
        const data = res.validate({
            contract_id: 'required'
        })
        if (data === false) return

        const {contract_id} = data

        try {

            const contract = await Repo.contract.findOrFail(contract_id)
            if (contract.run_end) return res.jerr('Esta corrida já terminou!')
            if (contract.run_ini) return res.jerr('Esta corrida já começou!')

            //
            await Repo.contract.start(contract.id)

            // status
            const affectedUserIds = [contract.ad_user_id, contract.driver_user_id]
            await Repo.user.statusContractRunning(affectedUserIds)
            SessionService.invalidateById(affectedUserIds)

            // notify
            const msg = `A corrida n.o ${contract_id} começou`
            MobileService.send(affectedUserIds, 'Corrida começou!', msg, 'contract_start').catch()

            //
            return res.jok()

        } catch (e) {
            return res.jerror(e)
        }
    },

    async finish(req, res)
    {
        const data = res.validate({
            contract_id: 'required'
        })
        if (data === false) return
        const {contract_id} = data

        try {

            const contract = await Repo.contract.findOrFail(contract_id)
            if (contract.run_end) return res.jerr('Esta corrida já terminou!')
            if ( ! contract.run_ini) return res.jerr('Esta corrida ainda não começou!')

            //
            await Repo.contract.finish(contract.id)

            // status
            const affectedUserIds = [contract.ad_user_id, contract.driver_user_id]
            await Repo.user.statusIdle(affectedUserIds)
            SessionService.invalidateById(affectedUserIds)

            // notify
            const msg = `A corrida n.o ${contract_id} terminou`
            MobileService.send(affectedUserIds, 'Corrida terminou!', msg, 'contract_finish').catch()

            //
            return res.jok()

        } catch (e) {
            return res.jerror(e)
        }
    }
}

module.exports = ApiContractController