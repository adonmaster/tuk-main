const Repo = require("../repos/Repo");
const MediaService = require("../services/MediaService");
const MobileService = require("../services/MobileService");


const ApiDriverController = {

    async myself(req, res) {
        try {
            const model = await Repo.driver.findByUserId(req.auth_id)
            if (model) return res.json(model.toJSON())
            return res.send('')
        } catch (e) {
            return res.status(400).send(e.message)
        }
    },

    async request(req, res)
    {
        const data = res.validate({
            avatar: 'required',
            avatar_thumb: 'required',
            avatar_uid: 'required',
            name_first: 'required',
            name_last: 'required',
            gender: 'required',
            birth: 'required',
            cpf: 'required',
            ref_city: 'required',
            ref_uf: 'required',
            license_no: 'required',
            license_valid_until: 'required',
            license_img: 'required',
            license_img_uid: 'required',
            legal_type: 'required',
            legal_img: 'required',
            legal_img_uid: 'required',
            car_brand: 'required',
            car_model: 'required',
            car_color: 'required',
            car_plate: 'required',
            car_img: 'required',
            car_img_uid: 'required',
            car_doc_img: 'required',
            car_doc_img_uid: 'required',
        })
        if (data === false) return

        try {

            const user_id = req.auth_id
            const { avatar, avatar_thumb, avatar_uid, name_first, name_last, gender, birth, cpf, ref_city, ref_uf, license_no, license_valid_until, license_img, license_img_uid, legal_type, legal_img, legal_img_uid, car_brand, car_model, car_color, car_plate, car_img, car_img_uid, car_doc_img, car_doc_img_uid } = data
            const request = await Repo.driverRequest.save(user_id, avatar, avatar_thumb, avatar_uid, name_first, name_last, gender, birth, cpf, ref_city, ref_uf, license_no, license_valid_until, license_img, license_img_uid, legal_type, legal_img, legal_img_uid, car_brand, car_model, car_color, car_plate, car_img, car_img_uid, car_doc_img, car_doc_img_uid)

            await MediaService.stashUidFlush()

            return res.jpay(request.toJSON())

        } catch (e) {
            return res.jerr(e.message)
        }

    },

    async requestValidate(req, res)
    {
        const data = res.validate({
            driver_request_id: 'required'
        })
        if (data === false) return

        //
        const { driver_request_id } = data
        try {

            const request = await Repo.driverRequest.findOrError(driver_request_id)

            //
            const driver = await Repo.transaction(async t => {

                const d = await Repo.driver.saveFrom(request, t)
                const car = await Repo.driver.saveCarFrom(d.id, request, t)
                await Repo.driver.updateDefCar(d.id, car.id, t)
                await Repo.driverRequest.closeIt(request.id, t)
                return d

            })

            //
            await MediaService.stashUidFlush()

            // notify
            await MobileService.send(
                [request.user_id],
                'Seja bem vindo, motorista!',
                'Sua requisição foi aceita, parabéns!',
                'driver_accepted'
            )

            return res.jpay(driver)

        } catch(e) {
            return res.jerror(e)
        }
    },

    async online(req, res) {

        const data = res.validate({
            'option': 'required|boolean'
        })
        if (data === false) return

        const {option} = data

        try {
            await Repo.driver.updateOnline(req.auth_id, option)
            return res.jok()
        } catch(e) {
            return res.jerr(e.message)
        }

    },
    async position(req, res)
    {
        const data = res.validate({
            'lat': 'required|numeric',
            'lng': 'required|numeric'
        })
        if (data === false) return

        const {lat, lng} = data

        try {
            await Repo.driver.updatePosition(req.auth_id, lat, lng)
            return res.jok()
        } catch(e) {
            return res.jerr(e.message)
        }
    }
}

module.exports = ApiDriverController