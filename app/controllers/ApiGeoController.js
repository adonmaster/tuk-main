const fetch = require('node-fetch')

module.exports = {

    async route(req, res, next) {
        const data = res.validate({
            'origin': 'required',
            'destination': 'required'
        })
        if (data === false) return

        const { origin, destination } = data
        let key = process.env.GOOGLE_MAPS_SERVER_KEY

        let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${key}`
        try {
            const r = await fetch(url)
            const json = await r.json()

            return res.jpay(json)
        } catch (err) {
            return res.jerr(err.message)
        }
    },

    async distance(req, res, next)
    {
        const data = res.validate({
            'origin': 'required',
            'destination': 'required'
        })
        if (data === false) return

        const { origin, destination } = data
        let key = process.env.GOOGLE_MAPS_SERVER_KEY

        const url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=${key}`
        try {
            const r = await fetch(url)
            const json = await r.json()

            return res.jpay(json)
        } catch (err) {
            return res.jerr(err.message)
        }
    },

    async full(req, res) {
        const data = res.validate({
            'origin': 'required',
            'destination': 'required'
        })
        if (data === false) return

        const { origin, destination } = data
        let key = process.env.GOOGLE_MAPS_SERVER_KEY

        try {

            const routeUrl = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${key}`
            let r = await fetch(routeUrl)
            const route = await r.json()

            const distanceUrl = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=${key}`
            r = await fetch(distanceUrl)
            const distance = await r.json()

            return res.jpay({ route, distance })

        } catch (e) {
            return res.jerror(e)
        }

    }

}