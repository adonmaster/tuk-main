const SessionService = require("../services/SessionService")

const Repo = require('../repos/Repo')

const EmailService = require("../services/GmailService")
const SubjectCodeRepo = require("../repos/SubjectCodeRepo")
const { digitsOnly } = require('../helpers/Sanitizer')

module.exports = {

    async register(req, res, next)
    {
        const data = res.validate({
            email: 'required|email'
        })
        if (data === false) return

        //
        const {email} = data

        // save database
        const registerModel = await SubjectCodeRepo.save(email)

        // send email
        const emailRes = await EmailService.sendRegistration(email, registerModel.code)
        if ( ! emailRes.isOk) return res.jerr(emailRes.msg)

        //
        return res.jok()
    },


    async check(req, res, next)
    {
        const data = res.validateAndSanitize({
            name: 'required',
            email: 'required|email',
            code: 'required'
        }, {
            code: digitsOnly
        })
        if (data === false) return
        const {email, name, code} = data

        try {

            const model = await SubjectCodeRepo.find(email)
            if ( ! model) return res.jerr('Registro não encontrado.', 404)

            if (model.code !== code) return res.jerr('Código incorreto.')

            if (model.expired_at < new Date()) return res.jerr('Registro expirado. faça-o novamente.')

            // create user
            const user = await Repo.user.save(email, name)
            session.invalidateById(user.id)

            //
            return res.jpay(user.toJSON())

        } catch(e) {
            next(e)
        }
    }

}