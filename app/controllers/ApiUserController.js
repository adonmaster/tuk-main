const Repo = require('../repos/Repo')
const MediaService = require("../services/MediaService");
const SessionService = require("../services/SessionService");
const { def } = require('../helpers/Sanitizer')


const ApiUserController = {

    async updateNameAvatar(req, res)
    {
        const data = res.validateAndSanitize({
            'name': 'required'
        }, {
            'avatar_full': def(null),
            'avatar_thumb': def(null),
            'avatar_uid': def(null)
        })
        if (data === false) return

        const { name, avatar_full, avatar_thumb, avatar_uid } = data

        try {

            await Repo.user.updateNameAvatar(req.auth_id, name, avatar_uid, avatar_full, avatar_thumb)

            session.invalidateById(req.auth_id)
            await MediaService.stashUidFlush()

            return res.jok()

        } catch (e) {
            return res.jerr(e.message)
        }

    },

    async mobileService(req, res)
    {
        const data = res.validate({
            'service': 'required',
            'token': 'required'
        })
        if (data === false) return

        const { service, token } = data

        try {

            await Repo.user.updateMobile(req.auth_user.id, service, token)
            session.invalidateByToken(token)

            return res.jok()

        } catch (e) {
            return res.jerr(e.message)
        }
    },

    async myself(req, res)
    {
        if (req.query['refresh'] == 'true')
        {
            try {

                SessionService.invalidateById(req.auth_id)

                let model = await Repo.user.findSafe(req.auth_id);

                return await res.json(model)

            } catch (e) {
                return res.jerror(e)
            }
        }

        return res.json(req.auth_user.p.toJsonSafe())
    },

    async status(req, res)
    {
        if (req.query['refresh'] == 'true')
        {
            try {

                SessionService.invalidateById(req.auth_id)

                let model = await Repo.user.findSafe(req.auth_id);

                return await res.send(model.status)

            } catch (e) {
                return res.jerror(e)
            }
        }

        return res.send(req.auth_user.status)
    }
}

module.exports = ApiUserController