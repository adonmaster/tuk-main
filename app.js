require('dotenv').config()

global.moment = require('moment')

// log
let Logger = require('./app/helpers/Logger');
Logger.init()
global.loge = Logger.e.bind(Logger)

//
const express = require('express')
const app = express()

// encoding
app.use(require('cors')())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//
const morgan = require('morgan')
app.use(morgan('tiny'))


// custom middleware
const responseValidationExt = require('./app/extensions/response-validation-ext')
responseValidationExt(app)

// Session
const SessionService = require('./app/services/SessionService')
global.session = SessionService


// ------ Routes ------
require('./admin').init(app)


app.use('/api',
    require('./app/routes/api'),
    require('./app/mid/api-error-mid'),
    require('./app/mid/not-found-mid')
)

app.use('/', (req, res, next) => {
    return res.jok()
})


// services
const GMailService = require("./app/services/GmailService")
GMailService.init()

const MobileService = require("./app/services/MobileService")
MobileService.init()

const AdDriverService = require('./app/services/AdDriverService')
AdDriverService.init()

const CronService = require('./app/services/CronService')
CronService.init()

//
app.once('SIGUSR2', async () => {
    process.kill(process.pid, 'SIGUSR2')
})

//
const port = process.env.PORT || 4001
app.listen(port, () => console.log(`Listening port http://localhost:${port}`))