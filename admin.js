const init = app => {
    const AdminJS = require('adminjs')
    const AdminJSExpress = require('@adminjs/express')
    const AdminJSSequelize = require('@adminjs/sequelize')
    AdminJS.registerAdapter(AdminJSSequelize)
    const { sequelize } = require('./app/models')
    const adminJs = new AdminJS({ databases: [sequelize], rootPath: '/admin', })
    const router = AdminJSExpress.buildRouter(adminJs)
    app.use(adminJs.options.rootPath, router)
}


module.exports = { init }