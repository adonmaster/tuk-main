const { should, expect } = require('chai')
should()

const LightCache = require('../app/helpers/LightCache')

describe('LightCache', () => {

    it('should assert something simple', () => {
        expect(1).to.be.equal(1)
    })

    it('should retrieve correctly only once if not invalidated', async () => {

        const cache = new LightCache()
        let signal = 0
        const v = await cache.retrieve('jeruza', () => new Promise((resolve, reject) => {
            signal++
            resolve(3)
        }))
        expect(v).to.be.equal(3)
        expect(signal).to.be.equal(1)

        let rr = await cache.retrieve('jeruza', () => new Promise(r => r(22)))
        expect(rr).to.be.equal(3)

        expect(signal).to.be.equal(1)
    })

    it('should invalidate', async () => {

        const cache = new LightCache()
        let signal = 0
        const v = await cache.retrieve('jeruza', () => new Promise((resolve, reject) => {
            signal++
            resolve(3)
        }))
        expect(v).to.be.equal(3)
        expect(signal).to.be.equal(1)

        //
        cache.invalidate('jeruza')

        let rr = await cache.retrieve('jeruza', () => new Promise(r => {
            signal++
            r(22)
        }))
        expect(rr).to.be.equal(22)

        expect(signal).to.be.equal(2)

        //
        cache.invalidate('gosth')

        rr = await cache.retrieve('jeruza', () => new Promise(r => {
            signal++
            r(23)
        }))
        expect(rr).to.be.equal(22)

        expect(signal).to.be.equal(2)


        //
        cache.invalidate()

        rr = await cache.retrieve('jeruza', () => new Promise(r => {
            signal++
            r(44)
        }))
        expect(rr).to.be.equal(44)

        expect(signal).to.be.equal(3)
    })

    it('should filter too', async () => {

        let signal = 0
        const cache = new LightCache()

        const funn = async (num) => {
            signal++
            return num
        }
        await cache.retrieve("um", () => funn(1) )
        await cache.retrieve("dois", () => funn(2) )
        await cache.retrieve("tres", () => funn(3) )
        await cache.retrieve("um", () => funn(1) )

        expect(signal).to.be.equal(3)

        cache.filter((key, payload) => payload !== 3)

        await cache.retrieve("um", () => funn(1) )
        await cache.retrieve("dois", () => funn(2) )
        let trinta = await cache.retrieve("tres", () => funn(30) )

        expect(signal).to.be.equal(4)
        expect(trinta).to.be.equal(30)
    })

})